from __future__ import absolute_import
import sys
import os
import __main__
from lib import control_cl
from lib import argument_parser

__author__ = "Ilya Kaznacheev, Andrey Danilin, Vadim Kozlov"
__copyright__ = 'T-Systems RUS, 2015'
__version__ = "0.1.15"


def run():
    set_workdir()
    arguments = parse_arguments(sys.argv)

    control = control_cl.Control(
        json_path=arguments.json,
        xml_path=arguments.xml
        )

    result = control.run_tests()

    control.return_data(result, path=arguments.response)


def parse_arguments(raw_args):
    """ parse command line arguments """
    parser = argument_parser.ArgParser(prog=raw_args[0], version=__version__)

    if len(raw_args) == 1:
        parser.print_help()
        sys.exit()

    arguments = parser.parse_args(raw_args[1:])

    return arguments


def set_workdir():
    """ change working directory
        to runner root folder
        if runner called from
        another directory """
    main_dir = os.path.dirname(os.path.realpath(__main__.__file__))
    if main_dir != os.getcwd():
        os.chdir(main_dir)

if __name__ == "__main__":
    run()
