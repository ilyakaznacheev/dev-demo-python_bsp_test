from selenium.webdriver.support.ui import Select
import time
from lib import testcase_basic

class Test_01(testcase_basic.PrimaryTest):
    """
    Test_01
    Andrey Danilin
    T-systems RUS
    2015
    Change user's email address
    """
    def runTest(self):
        #BLOCK: Open
        driver = self.driver
        driver.get(self.base_url)
        #BLOCK: Logon
        driver.find_element_by_id("sap-user").clear()
        driver.find_element_by_id("sap-user").send_keys(self.get_param("username"))
        driver.find_element_by_id("sap-password").clear()
        driver.find_element_by_id("sap-password").send_keys(self.get_param("password"))
        driver.find_element_by_id("b1").click()
        driver.switch_to_frame(0)  #IDE: " Unsupported command [selectFrame | 54D6D3FF107958E7E10000000AAF5DE7_A | ]"
        #BLOCK: Data add
        driver.find_element_by_id("subc1_Benutzerpflege").click() #BLOCK: Menu "Benutzerpflege"
        self.alert_check("Menu: Benutzerpflege")
        driver.find_element_by_id("subc1_useraendern").click()
        self.alert_check("Button: Benutzerpflege->Userandern")
        driver.find_element_by_id("subc1_e_mail").clear()
        driver.find_element_by_id("subc1_e_mail").send_keys(self.get_param("useremail"))
        driver.find_element_by_id("subc1_useraendern").click()
        self.alert_check("Button: Benutzerpflege->Speichern")
        #BLOCK: Exit
        # driver.find_element_by_css_selector("img[alt=\"Abmelden\"]").click()
        # self.close_alert_and_get_its_text()

class Test_02(testcase_basic.PrimaryTest):
    """
    Test_02
    Andrey Danilin
    T-systems RUS
    2015
    Search telekom customer by TKKUNNR 
    """
    def runTest(self):
        #BLOCK: Open
        driver = self.driver
        driver.get(self.base_url)
        #BLOCK: Logon
        driver.find_element_by_id("sap-user").clear()
        driver.find_element_by_id("sap-user").send_keys(self.get_param("username"))
        driver.find_element_by_id("sap-password").clear()
        driver.find_element_by_id("sap-password").send_keys(self.get_param("password"))
        driver.find_element_by_id("b1").click()
        driver.switch_to_frame(0) #IDE: " Unsupported command [selectFrame | 54D6D3FF107958E7E10000000AAF5DE7_A | ]"
        #BLOCK: Data add
        driver.find_element_by_id("subc1_Kundendaten").click() #BLOCK: Menu "Kundendaten"
        self.alert_check("Menu: Kunde")
        driver.find_element_by_xpath("(//img[@id='subc1_suche'])[2]").click()
        self.alert_check("Menu: Kundendaten")
        driver.find_element_by_id("subc1_ywvko").clear()
        driver.find_element_by_id("subc1_ywvko").send_keys(self.get_param("tkkunden"))
        driver.find_element_by_id("subc1_kusu2").click()
        self.alert_check("Button: Kundendaten->Suchen")
        #BLOCK: Exit
        # driver.find_element_by_css_selector("img[alt=\"Abmelden\"]").click()
        # self.close_alert_and_get_its_text()

class Test_03(testcase_basic.PrimaryTest):
    """
    Test_03
    Andrey Danilin
    T-systems RUS
    2015
    Create "Miete" contract 
    """
    def runTest(self):
        driver = self.driver
        driver.get(self.base_url)
        #BLOCK: Logon
        driver.find_element_by_id("sap-user").clear()
        driver.find_element_by_id("sap-user").send_keys(self.get_param("username"))
        driver.find_element_by_id("sap-password").clear()
        driver.find_element_by_id("sap-password").send_keys(self.get_param("password"))
        driver.find_element_by_id("b1").click()
        driver.switch_to_frame(0) #IDE: " Unsupported command [selectFrame | 54D6D3FF107958E7E10000000AAF5DE7_A | ]"
        driver.find_element_by_id("subc1_VertragAuftrag").click()
        driver.find_element_by_id("subc1_rechnung").click()
        #Unsupported command [selectWindow | name=sapPopupMainId_X2 | ]
        iframe = driver.find_element_by_id("sapPopupMainId_X0")
        driver.switch_to_frame(iframe)
        driver.find_element_by_css_selector("span").click()
        driver.switch_to.default_content()
        driver.switch_to_frame(0)
        #Unsupported command [selectWindow | name=sapPopupMainId_X2 | ]
        iframe = driver.find_element_by_id("sapPopupMainId_X1")
        driver.switch_to_frame(iframe)
        driver.find_element_by_css_selector("span").click()
        driver.switch_to.default_content()
        driver.switch_to_frame(0)
        #Unsupported command [selectWindow | name=sapPopupMainId_X2 | ]
        iframe = driver.find_element_by_id("sapPopupMainId_X2")
        driver.switch_to_frame(iframe)
        driver.find_element_by_css_selector("span").click()
        driver.switch_to.default_content()
        driver.switch_to_frame(0)
        self.alert_check("Menu: Vertrag/Auftrag->Miete->Neubereitstellung->anlegen")
        #fill data
        #KUNNR
        driver.find_element_by_id("subc1_tkkunnr").clear()
        driver.find_element_by_id("subc1_tkkunnr").send_keys(self.get_param("tkkunden"))
        #FKTO
        driver.find_element_by_id("subc1_fkto").clear()
        driver.find_element_by_id("subc1_fkto").send_keys(self.get_param("fkto"))
        #OK
        driver.find_element_by_id("subc1_ok").clear()
        driver.find_element_by_id("subc1_ok").send_keys(self.get_param("ok"))
        #Suche
        driver.find_element_by_id("subc1_vk_suchen").click()
        self.alert_check("Button: Miete->anlegen->Suchen")
        #ALNR
        driver.find_element_by_id("subc1_alnr").clear()
        driver.find_element_by_id("subc1_alnr").send_keys(self.get_param("anlalnr"))
        #Date 1
        driver.find_element_by_id("subc1_untdt").clear()
        driver.find_element_by_id("subc1_untdt").send_keys(self.get_param("vuntdat"))
        #LAUFK
        Select(driver.find_element_by_id("subc1_laufk")).select_by_visible_text(self.get_param("laufk"))
        driver.find_element_by_css_selector("option[value=\"YI\"]").click()
        #Date 2
        driver.find_element_by_id("subc1_dtmae").clear()
        driver.find_element_by_id("subc1_dtmae").send_keys(self.get_param("instterm"))
        #Text
        driver.find_element_by_id("subc1_vertragspartner_tray-exp").click()
        driver.find_element_by_id("subc1_vpzb").clear()
        driver.find_element_by_id("subc1_vpzb").send_keys(self.get_param("vertrtext"))
        #Next tab
        driver.find_element_by_id("subc1_aanlegen-itm-1-txt").click()
        self.alert_check("Tab: Miete->anlegen->Systemubersicht")
        i = 1
        while True:
            try:
                var_menge = self.vars["posmenge_"+str(i)]
                var_alnr = self.vars["posalnr_"+str(i)]
            except KeyError:
                break
            else:
                para_menge = "subc1_mdlauftrag_positionen[{number}].menge".format(number=i)
                para_alnr = "subc1_mdlauftrag_positionen[{number}].alnr".format(number=i)
                element = driver.find_element_by_id(para_alnr)
                element.click()
                element.clear()
                element.send_keys(var_alnr)
                element = driver.find_element_by_id(para_menge)
                element.click()
                element.clear()
                element.send_keys(var_menge)
                i += 1
        #Save items
        driver.find_element_by_id("subc1_btn_auftrag_save").click()
        #Ok
        self.close_alert_and_get_its_text()
        #Save docs
        driver.find_element_by_id("subc1_aanlegen-itm-2-txt").click() #go to kommerziellen Bedingungen
        self.alert_check("Tab: Miete->anlegen->kommerziellen Bedingungen")
        driver.find_element_by_id("subc1_btn_auftrag_save").click()
        self.alert_check("Tab: Miete->anlegen->kommerziellen Bedingungen->save")
        #Done
        self.resp_vars['vertrnr'] = driver.find_element_by_id("subc1_vertrg").get_attribute("value")
        self.resp_vars['tkkunden'] = driver.find_element_by_id("subc1_tkkunnr").get_attribute("value")
        #BLOCK: Exit
        driver.find_element_by_css_selector("img[alt=\"Abmelden\"]").click()
        self.close_alert_and_get_its_text()