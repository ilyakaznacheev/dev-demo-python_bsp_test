from selenium import webdriver
from selenium.webdriver.common.by import By
from lib import testcase_basic

class Testcases_reg_16(testcase_basic.PrimaryTest):
    """
    Regretion test 16
    Andrey Danilin
    T-systems RUS
    2015
    Storniere Kuendigung aus Testfall 15
    """
    def runTest(self):
        driver = self.driver
        driver.get(self.base_url)
        #BLOCK: Logon
        self.logon()
        self.click_and_go(self.get_elemet("subc1_VertragAuftrag"), "Menu:Vertrag")
        self.click_and_go(self.get_elemet("subc1_storno", how=By.ID), "Menu:Storno")
        #fill data
        #KUNNR
        self.send_value("tkkunden", "subc1_tkkunnr")
        #Vertrag
        self.send_value("vertrnr", "subc1_vertrg")
        #Nachtrag
        self.send_value("nachtrnr", "subc1_nchtrg", how=By.ID, not_req="X")
        #Suche
        self.click_and_go(self.get_elemet("subc1_vk_suchen", how=By.ID), "Button:Kuendigung->Suchen")
        #Grund
        self.select_value("storno_kz", "subc1_fv_kz", how=By.ID)
        #Next tab (Systemuebersicht)
        self.click_and_go(self.get_elemet("subc1_btn_storno_save", how=By.ID), "Tab:Storno->Save")
        self.logger.info("Auftrag storniert - Status 9999")
        #Done
        self.resp_vars['vertrnr'] = self.get_elemet("subc1_vertrg").get_attribute("value")
        self.resp_vars['tkkunden'] = self.get_elemet("subc1_tkkunnr").get_attribute("value")
        self.resp_vars['ok'] = self.get_elemet("subc1_ok").get_attribute("value")
        self.resp_vars['veraufnr'] = self.get_elemet("subc1_aufnr").get_attribute("value")
        self.resp_vars['nachtrnr'] = self.get_elemet("subc1_nchtrg").get_attribute("value")