import os

if __name__ != "__main__":
    exports = list()
    globals_, locals_ = globals(), locals()
    package_path = os.path.dirname(__file__)
    package_name = os.path.basename(package_path)

    for filename in os.listdir(package_path):
        modulename, ext = os.path.splitext(filename)
        if not modulename.startswith("_") and ext in ('.py', '.pyw'):
            # create a package relative subpackage name
            subpackage = '{}.{}'.format(package_name, modulename)
            module = __import__(subpackage, globals_, locals_, [modulename])
            modict = module.__dict__
            names = (modict['__all__'] if '__all__' in modict else
                     [name for name in modict if not name.startswith("_")])
            exports.extend(names)
            globals_.update((name, modict[name]) for name in names)

    __all__ = ["__all__"] + exports
