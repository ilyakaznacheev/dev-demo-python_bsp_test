from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from lib import testcase_basic

class Testcases_reg_4(testcase_basic.PrimaryTest):
    """
    Regretion test 4
    Andrey Danilin
    T-systems RUS
    2015
    regulierer Aendern. Aendere Hausnummer
    """
    def runTest(self):
        #BLOCK: Open
        driver = self.driver
        driver.get(self.base_url)
        try:
            #BLOCK: Logon
            self.logon()
            #BLOCK: Navi
            self.click_and_go(self.get_elemet("subc1_Kundendaten"), "Menu:Kundendaten")
            self.click_and_go(self.get_elemet("subc1_tr_krech"), "Menu:Regulierer")
            #Unsupported command [selectWindow | name=sapPopupMainId_X2 | ]
            self.popup_navigation(0, "MENU_REGULIERERBEARB_E", log_text="Menu:Andern")
            #BLOCK: Data add
            self.send_value("tkkunden", "subc1_ywvko")
            self.send_value("fkto", "subc1_fkto")
            self.click_and_go(self.get_elemet("subc1_kusu2"), "Button: Kunden Suchen")
            #Data
            self.send_value("tkkhausnr_reg", "subc1_yyghausnr")
            #Rechnungsanschrift aendern
            self.send_value("tkkname_reg", "subc1_name_1", how=By.ID, not_req="X")
            self.send_value("tkkname_2_reg", "subc1_name_2", how=By.ID, not_req="X")
            self.send_value("tkkvorname", "subc1_vorname", how=By.ID, not_req="X")
            self.select_value("tkkanrede_reg", "subc1_anred", not_req="X")
            self.select_value("tkktitle", "yygtitel", not_req="X")
            self.select_value("tkkhistzus", "l_yygnamehzs", not_req="X")
            self.send_value("tkkangabe", "subc1_name_4", how=By.ID, not_req="X")
            self.send_value("tkkstrass_reg", "subc1_yygstras01", how=By.ID, not_req="X")
            self.send_value("tkkpstlz_reg", "subc1_pstl1", how=By.ID, not_req="X")
            self.send_value("ttkpstlz_2", "subc1_pstl2", how=By.ID, not_req="X")
            self.send_value("tkkort_reg", "subc1_ort01", how=By.ID, not_req="X")
            self.select_value("tkkausland", "subc1_kzland", not_req="X")
            self.select_value("tkkzahlwe", "subc1_zahlwe", not_req="X")
            self.send_value("tkkelfe", "subc1_elfe", how=By.ID, not_req="X")
            self.select_value("tkkustkz", "subc1_ustkz", not_req="X")
            self.select_value("tkknamevw", "l_yygnamevs", not_req="X")
            self.select_value("tkknameszs", "l_yygnameszs", not_req="X")
            self.send_value("tkkhausnr_2_reg", "subc1_yyghausnrz", how=By.ID, not_req="X")
            self.send_value("tkkabrech", "subc1_abrechtag", how=By.ID, not_req="X")
            #Bankverbindung
            #Next Tab
            self.click_and_go(self.get_elemet("subc1_arechanschr-itm-1-txt"), "Tab: Bankverbindung aendern")
            self.select_value("tkkzahlweg", "subc1_zahlweg", not_req="X")
            #TODO: not_req for get_element ????
            try:
                self.click_and_go(self.get_elemet("subc1_mandrefnr_akt", no_log="X"), "Button: Bankdaten aendern")
            except NoSuchElementException:
                self.logger.info("Element \"{}\" not found!".format("subc1_mandrefnr_akt"))
            self.send_value("sepabic", "subc1_bic", how=By.ID, not_req="X")
            self.send_value("sepaiban", "subc1_iban", how=By.ID, not_req="X")
            self.send_value("sepamandnr", "subc1_mandref", how=By.ID, not_req="X")
            self.send_value("sepakontoin", "subc1_koinh", how=By.ID, not_req="X")
            # Save Data
            self.click_and_go(self.get_elemet("subc1_reganl"), "Button: Speichern")
            self.click_and_go(self.get_elemet("subc1_arechanschr-itm-0-txt"), "Tab: Rechnunganschrift")
            #Output data
            self.resp_vars['tc_kunnr_v'] = self.get_elemet("subc1_ywvko").get_attribute("value")
            self.resp_vars['tc_fkto_v'] = self.get_elemet("subc1_fkto").get_attribute("value")
            self.resp_vars['tkkklsid'] = self.get_elemet("subc1_klsid").get_attribute("value")
        except Exception, error:
            self.logger.critical("Fatal error: " + str(error))
            raise error