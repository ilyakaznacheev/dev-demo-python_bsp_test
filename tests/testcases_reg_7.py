from selenium import webdriver
from selenium.webdriver.common.by import By
from lib import testcase_basic

class Testcases_reg_7(testcase_basic.PrimaryTest):
    """
    Regretion test 7
    Andrey Danilin
    T-systems RUS
    2015
    Erfasse Ansprechpartner
    """
    def runTest(self):
        #BLOCK: Open
        driver = self.driver
        driver.get(self.base_url)
        #BLOCK: Logon
        self.logon()
        #BLOCK: Navi
        self.click_and_go(self.get_elemet("subc1_Kundendaten"), "Menu:Kundendaten")
        self.click_and_go(self.get_elemet("subc1_tr_standort"), "Menu:Standort")
        #Unsupported command [selectWindow | name=sapPopupMainId_X2 | ]
        self.popup_navigation(0, "MENU_STANDORTASP_E", log_text="Menu:Ansprechpartner")
        #BLOCK: Data add
        self.send_value("tkkunden", "subc1_ywvko")
        self.send_value("ok_asp", "subc1_ok")
        self.click_and_go(self.get_elemet("subc1_kusu2"), "Button: Kunden und ok Suchen")
        #Data
        # self.send_value("aspnr", "subc1_aspparnr")
        self.select_value("aspanrede", "subc1_aspanrede")
        self.send_value("aspname", "subc1_aspname")
        #not necessarily
        self.send_value("aspfkt", "subc1_aspfkt", how=By.ID, not_req="X")
        self.send_value("aspraum", "subc1_aspraum", how=By.ID, not_req="X")
        self.send_value("asptel", "subc1_asptel", how=By.ID, not_req="X")
        self.send_value("aspfax", "subc1_aspfax", how=By.ID, not_req="X")
        self.send_value("aspmail", "subc1_aspemail", how=By.ID, not_req="X")
        self.send_value("aspvorname", "subc1_aspvorname", how=By.ID, not_req="X")
        self.send_value("aspabt", "subc1_aspabt", how=By.ID, not_req="X")
        self.send_value("aspfkt", "subc1_aspstock", how=By.ID, not_req="X")
        self.send_value("aspgeb", "subc1_aspgeb", how=By.ID, not_req="X")
        self.send_value("asplteltext", "subc1_asptelext", how=By.ID, not_req="X")
        self.send_value("aspfaxtext", "subc1_aspfaxext", how=By.ID, not_req="X")
        #Save Data
        self.click_and_go(self.get_elemet("subc1_standortaspsave"), "Button: Speichern ASP")
        #Output data
        self.resp_vars['tkkunden'] = self.get_elemet("subc1_ywvko").get_attribute("value")
        self.resp_vars['ok'] = self.get_elemet("subc1_ok1").get_attribute("value")
        self.resp_vars['aspnr'] = self.get_elemet("subc1_aspparnr").get_attribute("value")