from selenium import webdriver
from selenium.webdriver.common.by import By
from lib import testcase_basic

class Testcases_reg_11(testcase_basic.PrimaryTest):
	"""
	Regretion test 11
	Andrey Danilin
	T-systems RUS
	2015
	Lege Miete Kuendigung. Buche durch bis Status 990
	"""
	def runTest(self):
		driver = self.driver
		driver.get(self.base_url)
		#BLOCK: Logon
		self.logon()
		self.click_and_go(self.get_elemet("subc1_VertragAuftrag"), "Menu:Vertrag")
		self.click_and_go(self.get_elemet("subc1_tr_vakuen", how=By.ID), "Menu:Kuendigung")
		#Unsupported command [selectWindow | name=sapPopupMainId_X2 | ]
		self.popup_navigation(0, "i_kuenmiet", log_text="Menu:Kuendigung->Miete")
		#Unsupported command [selectWindow | name=sapPopupMainId_X2 | ]
		self.popup_navigation(1, "MENU_MIET_KUEN_N", log_text="Menu:Kuendigung->Miete->anlegen")
		#fill data
		#KUNNR
		self.send_value("tkkunden", "subc1_tkkunnr")
		#Vertrag
		self.send_value("vertrnr", "subc1_vertrg")
		#Suche
		self.click_and_go(self.get_elemet("subc1_vk_suchen", how=By.ID), "Button:Kuendigung->Suchen")
		#Data kuendigung
		self.send_value("kuenddate", "subc1_subc2_keindt")
		#Grund
		self.select_value("kuendgrund", "subc1_subc2_kuegru", how=By.ID)
		self.send_value("kuendwirk", "subc1_subc2_kwndat")
		self.send_value("endepreisdat", "subc1_subc2_endprf")
		#not neccessarily
		self.send_value("demtermdat", "subc1_subc2_dtmae", how=By.ID, not_req="X")
		self.send_value("beratertele", "subc1_subc2_berater", how=By.ID, not_req="X")
		#Text
		if self.select_radiobutton("vertrtext", "subc1_subc2_vertragspartner_tray-exp", not_req="X"):
			self.send_value("vertrtext", "subc1_subc2_vpzb", how=By.ID, not_req="X")
		#not necessarily
		# self.send_value("vertbez", "subc1_subc2_vertragsbeziehungen", how=By.ID, not_req="X")
		if self.select_radiobutton("nbab", "subc1_subc2_nebenabreden_tray-exp", not_req="X"):
			self.send_value("nbab", "subc1_subc2_nbab", how=By.ID, not_req="X")
		if self.select_radiobutton("bmkg", "subc1_subc2_bemerkungen_tray-exp", not_req="X"):  
			self.send_value("bmkg", "subc1_subc2_bmkg", how=By.ID, not_req="X")
		#Next tab (Installationskosten)
		self.click_and_go(self.get_elemet("subc1_kueanlegen-itm-1-txt", how=By.ID), "Tab:kuendigung->Installationskosten")
		#not necessarily
		self.select_value("tk_kz", "subc1_tk_kz", how=By.ID, not_req="X")
		self.select_value("ltg_kz", "subc1_ltg_kz", how=By.ID, not_req="X")
		self.select_value("fv_kz", "subc1_fv_kz", how=By.ID, not_req="X")
		self.send_value("tkvereinblaut", "subc1_tkanlvereinblautvertr", how=By.ID, not_req="X")
		self.send_value("ltgvereinblaut", "subc1_ltgnvereinblautvertr", how=By.ID, not_req="X")
		self.send_value("fvvereinblaut", "subc1_fvvereinblautvertr", how=By.ID, not_req="X")
		#Go to Abloesebetraege    
		self.click_and_go(self.get_elemet("subc1_kueanlegen-itm-3-txt", how=By.ID), "Button:kuendigung->Abloesebetraege")
		#not necessarily
		self.send_value("ablabwntrag", "subc1_ABL_ABW_BETRAG", how=By.ID, not_req="X")
		self.select_value("abl_alnr", "subc1_mdlauftrag_abloesebetraege.abl_alnr", how=By.ID, not_req="X")
		self.select_value("abl_nea_alnr", "subc1_mdlauftrag_abloesebetraege.abl_nea_alnr", how=By.ID, not_req="X")
		#not necessarily
		self.select_radiobutton("ratenzahl", "subc1_ABL_RATENZAHLUNG-img", how=By.ID, not_req="X", log_text="Ratenzahlung")
		self.send_value("betrmonat", "subc1_ABL_RATE1", how=By.ID, not_req="X")
		self.send_value("anzahlfoldmon", "subc1_ABL_ANZAHL_RATEN", how=By.ID, not_req="X")
		#Aktualisieren
		self.click_and_go(self.get_elemet("subc1_btn_abl_aktualisieren", how=By.ID), "Button:Kuendigung->Abloesebetraege->Aktualisieren")
		#Save
		self.click_and_go(self.get_elemet("subc1_btn_auftrag_save", how=By.ID), "Button:Kuendigung->Abloesebetraege->Save")
		#Next tab (Dokumente / MtA)
		self.click_and_go(self.get_elemet("subc1_kueanlegen-itm-2-txt", how=By.ID), "Button:Kuendigung->Dokumente / MtA")
		#not necessarily
		if self.select_radiobutton("kundansch_tch", "subc1_kundenanschrift-exp", how=By.ID, not_req="X"):
			self.send_value("ansch_anrede", "subc1_anred", how=By.ID, not_req="X")
			self.send_value("ansch_name", "subc1_name1", how=By.ID, not_req="X")
			# self.send_value("ansch_name2", "subc1_name2", how=By.ID, not_req="X")
			self.send_value("ansch_vorname", "subc1_name3", how=By.ID, not_req="X")
			self.send_value("ansch_strasse", "subc1_stras", how=By.ID, not_req="X")
			self.send_value("ansch_hausnummer", "subc1_hsnmr", how=By.ID, not_req="X")
			self.send_value("ansch_hauszus", "subc1_hsnmrzus", how=By.ID, not_req="X")
			# self.send_value("ansch_postleitzahl", "subc1_pstlz", how=By.ID, not_req="X")
			# self.send_value("ansch_ort", "subc1_ort", how=By.ID, not_req="X")
		#Next!!! Textbausteine Kundenanschreiben
		if self.select_radiobutton("kundanschtxt_tch", "subc1_kundenanschreiben-exp", how=By.ID, not_req="X"):    #Kundenanschreiben
			self.send_value("anschtxt_uzei", "subc1_uzei", how=By.ID, not_req="X")
			self.send_value("anschtxt_duwa", "subc1_duwa", how=By.ID, not_req="X")
			self.send_value("anschtxt_betr", "subc1_betr", how=By.ID, not_req="X")
			self.send_value("anschtxt_betr2", "subc1_betr2", how=By.ID, not_req="X")
			self.send_value("anschtxt_betr3", "subc1_betr3", how=By.ID, not_req="X")
			self.send_value("anschtxt_zaft", "subc1_zaft", how=By.ID, not_req="X")
		#Next!!! Kuendigung
		if self.get_param("kuendigungtxt_tch", "X"):    #Kuendigung
			self.click_and_go(self.get_elemet("subc1_kuendigung_kunde-exp", how=By.ID), "Kuendigung")
			if self.select_radiobutton("kuenddkunde", "subc1_mdlauftrag_kdanschr.kuenart1__subc1_kuenKunde-img", how=By.ID, not_req="X"):
				if self.select_radiobutton("kuendvorzzeitig", "subc1_mdlauftrag_kdanschr.kuenart2__subc1_kuenArt1-img", how=By.ID, not_req="X"):
					self.select_value("list_box_1", "subc1_myDropdownListBox1", how=By.ID, not_req="X")
				if self.select_radiobutton("kuendtermin", "subc1_mdlauftrag_kdanschr.kuenart2__subc1_kuenArt2-img", how=By.ID, not_req="X"):
					self.select_value("list_box_2", "subc1_myDropdownListBox2", how=By.ID, not_req="X")
			if self.select_radiobutton("kuenddtcom", "subc1_mdlauftrag_kdanschr.kuenart1__subc1_kuenTCom-img", how=By.ID, not_req="X"):
				self.select_radiobutton("kuendvorzzeitig", "subc1_mdlauftrag_kdanschr.kuenart2__subc1_kuenArt1-img", how=By.ID, not_req="X")
				if self.select_radiobutton("kuendtermin", "subc1_mdlauftrag_kdanschr.kuenart2__subc1_kuenArt2-img", how=By.ID, not_req="X"):
					self.select_value("list_box_2", "subc1_myDropdownListBox2", how=By.ID, not_req="X")
			self.click_and_go(self.get_elemet("subc1_gen_kuend_schreiben", how=By.ID), "Kuendigung-text")
		#Next!!!Unterschriftsregelung
		if self.get_param("unterschrift_tch", "X"):
			self.click_and_go(self.get_elemet("subc1_unterschriftsregelung-exp", how=By.ID), "Unterschriftsregelung")
			self.select_radiobutton("unterschrift_fnk_1", "subc1_mdlauftrag_kdanschr.zart__subc1_zart_btn1-img", how=By.ID, not_req="X")
			self.select_radiobutton("unterschrift_fnk_2", "subc1_mdlauftrag_kdanschr.zart__subc1_zart_btn2-img", how=By.ID, not_req="X")
			self.select_radiobutton("unterschrift_fnk_3", "subc1_mdlauftrag_kdanschr.zart__subc1_zart_btn3-img", how=By.ID, not_req="X")
			self.select_radiobutton("unterschrift_opt11", "subc1_mdlauftrag_kdanschr.usrg1__subc1_usrg1_btn1-img", how=By.ID, not_req="X")
			self.select_radiobutton("unterschrift_opt12", "subc1_mdlauftrag_kdanschr.usrg1__subc1_usrg1_btn2-img", how=By.ID, not_req="X")
			self.select_radiobutton("unterschrift_opt13", "subc1_mdlauftrag_kdanschr.usrg1__subc1_usrg1_btn3-img", how=By.ID, not_req="X")
			self.select_radiobutton("unterschrift_opt14", "subc1_mdlauftrag_kdanschr.usrg1__subc1_usrg1_btn4-img", how=By.ID, not_req="X")
			#Data
			self.send_value("unterschrift_unte1", "subc1_unte1", how=By.ID, not_req="X")
			#21
			self.select_radiobutton("unterschrift_opt21", "subc1_mdlauftrag_kdanschr.usrg2__subc1_usrg2_btn1-img", how=By.ID, not_req="X")
			self.select_radiobutton("unterschrift_opt22", "subc1_mdlauftrag_kdanschr.usrg2__subc1_usrg2_btn2-img", how=By.ID, not_req="X")
			self.select_radiobutton("unterschrift_opt23", "subc1_mdlauftrag_kdanschr.usrg2__subc1_usrg2_btn3-img", how=By.ID, not_req="X")
			self.select_radiobutton("unterschrift_opt24", "subc1_mdlauftrag_kdanschr.usrg2__subc1_usrg2_btn4-img", how=By.ID, not_req="X")
			#Data
			self.send_value("unterschrift_unte2", "subc1_unte2", how=By.ID, not_req="X")
		#next!!! generierter Textblock
		if self.get_param("txtblock_tch", "X"):    #Kundenanschrift
			self.click_and_go(self.get_elemet("subc1_gentextblock-exp", how=By.ID), "generierter Textblock")
			self.send_value("txtblock_zvbp", "subc1_zvbp", how=By.ID, not_req="X")
			self.select_radiobutton("txtblock_save", "subc1_gen_kuend_schreiben", how=By.ID, not_req="X")
			self.select_radiobutton("txtblock_del", "subc1_gen_kuend_delete", how=By.ID, not_req="X")
		#Save
		self.click_and_go(self.get_elemet("subc1_btn_auftrag_save", how=By.ID), "Button:Kuendigung->Dokumente/MtA->save")
		#Montage-/Serviceauftrag
		self.click_and_go(self.get_elemet("subc1_mdlauftrag_kdanschr.radiogrp1__subc1_btn2-img", how=By.ID), "Button:Kuendigung->Montage-/Serviceauftrag")
		#not necessarily
		if self.select_radiobutton("hinweis_serv", "subc1_hinwServ-exp", not_req="X"):
			self.send_value("hinweis_serv", "subc1_hwsc", how=By.ID, not_req="X")
		if self.select_radiobutton("hinweis_fertig", "subc1_hinweisfertigung_tray-exp", not_req="X"):
			self.send_value("hinweis_fertig", "subc1_hinweisfertigung", how=By.ID, not_req="X")
		#Sent mail
		if self.get_param("montfrei_mail_need", "X"):
			self.click_and_go(self.get_elemet("subc1_dok_mail", how=By.ID), "Button:Kuendigung->Montage-/Serviceauftrag->Mail")
		# self.send_value("mont_rufnr", "subc1_beschrufnr", how=By.ID, not_req="X")
		#Montageauftrag freigabe -> status 810
		self.click_and_go(self.get_elemet("subc1_freigabe", how=By.ID), "Button:Kuendigung->Montage-/Serviceauftrag->Freigabe")
		self.logger.info("Status 810 gesetzt, Montageauftrag angelegt")
		#Montage auftrag
		#Go to Real. auftrag
		self.click_and_go(self.get_elemet("subc1_RealisierungsAuftrag", how=By.ID), "Button: Real. auftrag")
		#Data
		self.send_value("mntinstdt", "subc1_m_insdt")
		self.get_elemet("subc1_hinweisfertigung_tray-exp").click()
		self.send_value("mnthinweisfert", "subc1_hinweisfertigung")
		#not necessarily
		self.send_value("mntbeschrufnr", "subc1_beschrufnr", how=By.ID, not_req="X")
		if self.select_radiobutton("mnthinweisabr", "subc1_hinweisabrechnung_tray-exp", not_req="X"):
			self.send_value("mnthinweisabr", "subc1_hinweisabrechnung", how=By.ID, not_req="X")
		if self.select_radiobutton("mnthinweisserv", "subc1_hinwServ-exp", not_req="X"):
			self.send_value("mnthinweisserv", "subc1_hwsc", how=By.ID, not_req="X")
		#ABBV Text
		#Aufrtagpos text
		if self.get_param("mntabbvpos_tch", "X"):
			self.get_elemet("(//a[@id='subc1_abbv_txt_pflegen']/span)[1]", how=By.XPATH).click()
			#Get type
			self.select_radiobutton("mntabbvpos_clk_1", "subc1_mdlauftrag_abbv_txt.radiotext__subc1_kuntext_einmalig-img", how=By.ID, not_req="X")
			self.select_radiobutton("mntabbvpos_clk_2", "subc1_mdlauftrag_abbv_txt.radiotext__subc1_kuntext_wiederkehrend-img", how=By.ID, not_req="X")
			self.select_radiobutton("mntabbvpos_clk_3", "subc1_mdlauftrag_abbv_txt.radiotext__subc1_kuntext_intervall-img", how=By.ID, not_req="X")
			i = 1
			while True:
				if not self.send_value("mntabbvpos_txt_"+str(i), "subc1_vertragtext_ver"+str(i), how=By.ID, not_req="X"):
					break 
				i += 1
			self.send_value("mntabbvpos_int_anz", "subc1_kuntext_intervall_anz", how=By.ID, not_req="X")
			#Close
			self.click_and_go(self.get_elemet("subc1_close", how=By.ID), "Button: ABBV-Text close")
		#Auftrag text
		if self.get_param("mntabbvauf_tch", "X"):
			self.get_elemet("(//a[@id='subc1_abbv_txt_pflegen']/span)[2]", how=By.XPATH).click()
			#Get type
			self.select_radiobutton("mntabbvpos_clk_1", "subc1_mdlauftrag_abbv_txt.radiotext__subc1_kuntext_einmalig-img", how=By.ID, not_req="X")
			self.select_radiobutton("mntabbvpos_clk_2", "subc1_mdlauftrag_abbv_txt.radiotext__subc1_kuntext_wiederkehrend-img", how=By.ID, not_req="X")
			self.select_radiobutton("mntabbvpos_clk_3", "subc1_mdlauftrag_abbv_txt.radiotext__subc1_kuntext_intervall-img", how=By.ID, not_req="X")
			i = 1
			while True:
				if not self.send_value("mntabbvpos_txt_"+str(i), "subc1_vertragtext_ver"+str(i), how=By.ID, not_req="X"):
					break
				i += 1
			self.send_value("mntabbvpos_int_anz", "subc1_kuntext_intervall_anz", how=By.ID, not_req="X")
			#Close
			self.click_and_go(self.get_elemet("subc1_close", how=By.ID), "Button: ABBV-Text close")
		#Go to Montagekosten
		self.click_and_go(self.get_elemet("subc1_rauf-itm-1-txt", how=By.ID), "Tab: Montagekosten")
		i = 1
		while True:
			if not self.send_value("mntmenge_"+str(i), "subc1_mdlauftrag_ref_tab_montkost[{number}].menge".format(number=i), not_req="X"): break
			if not self.send_value("mntalnr_"+str(i), "subc1_mdlauftrag_ref_tab_montkost[{number}].alnr".format(number=i), not_req="X"): break
			self.click_and_go(self.get_elemet("subc1_pos_pruefen", how=By.ID), "Tab: Montagekosten->Pruefe", l_noerror=True)
			#Price - inactive - no error
			self.send_value("mntprice_"+str(i), "subc1_mdlauftrag_ref_tab_montkost[1].lpreis".format(number=i), not_req="X")
			self.send_value("mnttxt_"+str(i), "subc1_mdlauftrag_ref_tab_montkost[{number}].text".format(number=i), not_req="X")
			i += 1
		self.click_and_go(self.get_elemet("subc1_save_rauftrag", how=By.ID), "Button: Montagekosten->Save")
		#Go to Installationskosten
		self.click_and_go(self.get_elemet("subc1_rauf-itm-2-txt", how=By.ID), "Tab:  Installationskosten")
		#not necessarily
		self.select_value("mnttk_kz", "subc1_tk_kz", how=By.ID, not_req="X")
		self.select_value("mntltg_kz", "subc1_ltg_kz", how=By.ID, not_req="X")
		self.select_value("mntfv_kz", "subc1_fv_kz", how=By.ID, not_req="X")
		#data
		self.send_value("mnttkvereinblaut", "subc1_tkanlvereinblautvertr", how=By.ID, not_req="X")
		self.send_value("mntltgvereinblaut", "subc1_ltgnvereinblautvertr", how=By.ID, not_req="X")
		self.send_value("mntfvvereinblaut", "subc1_fvvereinblautvertr", how=By.ID, not_req="X")
		#Save
		self.click_and_go(self.get_elemet("subc1_save_rauftrag", how=By.ID), "Tab: Installationskosten->Save")
		#Go to Abloesebetraege
		self.click_and_go(self.get_elemet("subc1_rauf-itm-3-txt", how=By.ID), "Tab: Abloesebetraege")
		#not necessarily
		self.select_radiobutton("mntratenzahl", "subc1_ABL_RATENZAHLUNG-img", how=By.ID, not_req="X")
		self.send_value("mntbetrmonat", "subc1_ABL_RATE1", how=By.ID, not_req="X")
		self.send_value("mntanzahlfoldmon", "subc1_ABL_ANZAHL_RATEN", how=By.ID, not_req="X")
		#Aktualisieren
		self.click_and_go(self.get_elemet("subc1_btn_abl_aktualisieren", how=By.ID), "Button: Aktualisieren")
		#Save
		self.click_and_go(self.get_elemet("subc1_save_rauftrag", how=By.ID), "Button: Abloesebetraege->Save")
		#Go to back TAB Grunddaten    
		self.click_and_go(self.get_elemet("subc1_rauf-itm-0-txt", how=By.ID), "Tab: TAB Grunddaten")
		#Close mobntage auftrag Status 820
		self.select_radiobutton("mntaufgeschl", "subc1_auftrabgeschl-img", how=By.ID, not_req="X")
		self.click_and_go(self.get_elemet("subc1_save_rauftrag", how=By.ID), "Button: TAB Grunddaten->Save")
		self.check_div_popup(popup_ids="ok_popup", button_class="urBtnStd urV", button_ids="subc1_install_ok")
		
		self.logger.info("Montageauftrag erledigt - Status 820")
		#Go to Verrechnungsauftrag
		self.click_and_go(self.get_elemet("subc1_tr_realaufverr", how=By.ID), "Menu: Verrechnungsauftrag")
		#Navi
		self.popup_navigation(0, "MENU_VERR_DUMY_E", log_text="Menu:Verrechnungsauftrag->aendern")
		#go to tab Datenuebergabe Abrechnung
		self.click_and_go(self.get_elemet("subc1_rauf-itm-1-txt", how=By.ID), "Tab: Verrechnungsauftrag->Datenuebergabe Abrechnung")
		self.click_and_go(self.get_elemet("subc1_dok_anz", how=By.ID), "Tab: Verrechnungsauftrag->Mail an Anwender")
		self.click_and_go(self.get_elemet("subc1_erled", how=By.ID), "Tab: Verrechnungsauftrag->Datenuebergabe starten")
		self.logger.info("Auftrag abgeschlossen - Status 990")
		#Done
		self.resp_vars['vertrnr'] = self.get_elemet("subc1_vertrg").get_attribute("value")
		self.resp_vars['tkkunden'] = self.get_elemet("subc1_tkkunnr").get_attribute("value")
		self.resp_vars['ok'] = self.get_elemet("stdot").get_attribute("value")
		self.resp_vars['veraufnr'] = self.get_elemet("subc1_aufnr").get_attribute("value")
		self.resp_vars['nachtrnr'] = self.get_elemet("subc1_nchtrg").get_attribute("value")