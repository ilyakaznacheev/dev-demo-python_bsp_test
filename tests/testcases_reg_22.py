from selenium import webdriver
from selenium.webdriver.common.by import By
from lib import testcase_basic

class Testcases_reg_22(testcase_basic.PrimaryTest):
    """
    Regretion test 22
    Andrey Danilin
    T-systems RUS
    2015
    Pruefe Kundendatenblatt fuer die angelegten Vertraege
    """
    def runTest(self):
        #BLOCK: Open
        driver = self.driver
        driver.get(self.base_url)
        #BLOCK: Logon
        self.logon()
        #BLOCK: Navi
        self.click_and_go(self.get_elemet("subc1_Kundendaten"), "Menu:Kundendaten")
        #go to blatt
        self.click_and_go(self.get_elemet("subc1_kundendatenblatt"), "Menu:Kundendatenblatt")
        #Suche
        self.send_value("tkkunden", "subc1_ywvko")
        self.send_value("fkto_ref", "subc1_fkto")
        # self.send_value("ok", "subc1_stdort")
        #Suche
        self.click_and_go(self.get_elemet("subc1_kusu2"), "Button: Kundensuchen")
        i = 1
        while True:
            if not self.send_value("vertrnr_"+str(i), "subc1_kundendbl$filter_1", how=By.ID, not_req="X"):
                break
            self.send_value("nachtrnr_"+str(i), "subc1_kundendbl$filter_2", how=By.ID, not_req="X")
            self.click_and_go(self.get_elemet("subc1_kundendbl-filterEvt"), "Menu:Filter")
            self.resp_vars['vertrnr_'+str(i)] = self.get_elemet("span.urTxtStd.urVt1", how=By.CSS_SELECTOR).text
            self.resp_vars['nachtrnr_'+str(i)] = self.get_elemet("//table[@id='subc1_kundendbl']/tbody/tr/td/table/tbody/tr[3]/td[3]/div/span", how=By.XPATH).text
            i += 1