from selenium import webdriver
from selenium.webdriver.common.by import By
from lib import testcase_basic

class Testcases_reg_12(testcase_basic.PrimaryTest):
    """
    Regretion test 12
    Andrey Danilin
    T-systems RUS
    2015
    Lege Gutschrift auf Mietvertrag an.
    """
    def runTest(self):
        driver = self.driver
        driver.get(self.base_url)
        self.logon()
        #Navigation
        self.click_and_go(self.get_elemet("subc1_VertragAuftrag", how=By.ID), "Menu:Vertrag")
        self.click_and_go(self.get_elemet("subc1_tr_vabear", how=By.ID), "Menu:Bearebeitung")
        #Unsupported command [selectWindow | name=sapPopupMainId_X2 | ]      
        self.popup_navigation(0, "i_bearbmiet", log_text="Menu:Bearbeitung->Miete")
        #Unsupported command [selectWindow | name=sapPopupMainId_X2 | ]
        self.popup_navigation(1, "i_bearbmietguts", log_text="Menu:Bearbeitung->Miete->Gutschrift")
        #Unsupported command [selectWindow | name=sapPopupMainId_X2 | ]
        self.popup_navigation(2, "MENU_MIET_GUTS_N", log_text="Menu:Bearbeitung->Miete->Gutschrift->anlegen")
        #fill data
        #KUNNR
        self.send_value("tkkunden", "subc1_tkkunnr")
        #Vertrag
        self.send_value("vertrnr", "subc1_vertrg")
        #Nachtrag
        self.send_value("nachtrnr", "subc1_nchtrg")
        #Suche
        self.click_and_go(self.get_elemet("subc1_vk_suchen", how=By.ID), "Button:Gutschrift->Suchen")
        #Data Beginn Preispflicht
        self.send_value("beginnpreisdat", "subc1_begprpflicht")        
        self.send_value("vertragbezeich", "subc1_mdlauftrag_kopf.yygvertbez", how=By.NAME, not_req="X")
        self.send_value("beratertele", "subc1_berater", not_req="X")
        #Text
        self.get_elemet("subc1_hinweisabrechnung_tray-exp", how=By.ID).click()
        self.send_value("vertrtext", "subc1_hinweisabrechnung", not_req="X")
        #Next tab Positionen
        self.click_and_go(self.get_elemet("subc1_aanlegen-itm-1-txt", how=By.ID), "Tab:Gutschrift->Positionen")
        #not necessarily (TEXT ABBV)
        i = 1
        while True:
            if not self.send_value("pos_menge_"+str(i), "subc1_mdlauftrag_positionen[{number}].menge".format(number=i), not_req="X"): break
            if not self.select_value("pos_alnr_"+str(i), "subc1_mdlauftrag_positionen[{number}].alnr".format(number=i), how=By.ID, not_req="X"): break
            self.click_and_go(self.get_elemet("subc1_pos_pruefen", how=By.ID), "Tab: Miete->Position->Pruefe", l_noerror=True)
            if not self.send_value("posprice_"+str(i), "subc1_mdlauftrag_positionen[{number}].nettopreis".format(number=i), not_req="X") : break
            #Check
            self.click_and_go(self.get_elemet("subc1_pos_pruefen", how=By.ID), "Position Pruefen")
            #TEXT
            param = self.get_param("pos_text_tch")
            if param:
                self.click_and_go(self.get_elemet("subc1_mdlauftrag_positionen[{}].flg_abbv_p".format(i), how=By.ID), "ABBV Text")
                #Get type   
                self.select_radiobutton("pos_text_clk_{}_1".format(i), "subc1_mdlauftrag_abbv_txt.radiotext_montpos__subc1_kuntext_einmalig-img", not_req="X")
                self.select_radiobutton("pos_text_clk_{}_2".format(i), "subc1_mdlauftrag_abbv_txt.radiotext_montpos__subc1_kuntext_wiederkehrend-img", not_req="X")
                self.select_radiobutton("pos_text_clk_{}_3".format(i), "subc1_mdlauftrag_abbv_txt.radiotext_montpos__subc1_kuntext_intervall-img", not_req="X")
                j = 1
                while True:
                    if not self.send_value("pos_text_abbv_{}_".format(i)+str(j), "subc1_vertragtext_ver"+str(j), not_req="X"):
                        break
                    j += 1
                self.send_value("pos_text_intertval_"+str(i), "subc1_kuntext_intervall_anz", not_req="X")
                #Close
                self.click_and_go(self.get_elemet("subc1_ueb", how=By.ID), "Button: ABBV-Text close")
            i += 1
        #Save
        self.click_and_go(self.get_elemet("subc1_save_gut_lastschrift", how=By.ID), "Button:Gutschrift->Save")
        #Go to Grunddaten 
        self.click_and_go(self.get_elemet("subc1_aanlegen-itm-0-txt", how=By.ID), "Tab:Gutschrift->Grunddaten")
        #Freigabe
        self.click_and_go(self.get_elemet("subc1_freigabe_gut_lastschrift", how=By.ID), "Button:Gutschrift->Grunddaten->Freigabe")
        self.logger.info("Gutschrift -> Status 820")
        #Status 820
        #Go to Verrechnungsauftrag
        self.click_and_go(self.get_elemet("subc1_RealisierungsAuftrag", how=By.ID), "Menu: RealisierungsAuftrag")
        self.click_and_go(self.get_elemet("subc1_tr_realaufverr", how=By.ID), "Menu: Verrechnungsauftrag")
        #navi
        self.popup_navigation(0, "MENU_VERR_DUMY_E", log_text="Menu:Verrechnungsauftrag->aendern")
        #go to tab Datenuebergabe Abrechnung
        self.click_and_go(self.get_elemet("subc1_rauf-itm-1-txt", how=By.ID),"Tab:Datenuebergabe Abrechnung")
        self.click_and_go(self.get_elemet("subc1_dok_anz"),"Button:Mail an Anwender")
        self.click_and_go(self.get_elemet("subc1_erled"),"Button:Datenuebergabe starten")
        self.logger.info("Gutschrift -> Status 990")
        #Done
        self.resp_vars['vertrnr'] = self.get_elemet("subc1_vertrg", how=By.ID).get_attribute("value")
        self.resp_vars['tkkunden'] = self.get_elemet("subc1_tkkunnr", how=By.ID).get_attribute("value")
        self.resp_vars['ok'] = self.get_elemet("stdot", how=By.ID).get_attribute("value")
        self.resp_vars['veraufnr'] = self.get_elemet("subc1_aufnr", how=By.ID).get_attribute("value")
        self.resp_vars['nachtrnr'] = self.get_elemet("subc1_nchtrg", how=By.ID).get_attribute("value")
        self.resp_vars['sdbelegnr'] = self.get_elemet("subc1_vbeln", how=By.ID).get_attribute("value") 