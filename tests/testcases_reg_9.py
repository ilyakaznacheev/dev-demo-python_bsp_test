from selenium import webdriver
from selenium.webdriver.common.by import By
from lib import testcase_basic

class Testcases_reg_9(testcase_basic.PrimaryTest):
    """
    Regretion test 9
    Andrey Danilin
    T-systems RUS
    2015
    Lege Miete Neubereitstellung an ueber web4al. Buche durch bis Status 990
    """
    def runTest(self):
        driver = self.driver
        driver.get(self.base_url)
        #BLOCK: Logon
        self.logon()
        #Get setting
        abrkenn = self.get_param("mntabrkenn")[:2]
        #Menu        
        self.click_and_go(self.get_elemet("subc1_VertragAuftrag"), "Menu:Vertrag")
        self.click_and_go(self.get_elemet("subc1_rechnung", how=By.ID), "Menu:Miete")
        #Unsupported command [selectWindow | name=sapPopupMainId_X2 | ]
        self.popup_navigation(0, "i_bearbmiet", log_text="Menu:Miete")
        #Unsupported command [selectWindow | name=sapPopupMainId_X2 | ]
        self.popup_navigation(1, "i_bearbmietneub", log_text="Menu:Miete")
        #Unsupported command [selectWindow | name=sapPopupMainId_X2 | ]
        self.popup_navigation(2, "MENU_MIET_NEUB_N", log_text="Menu:Miete")
        #fill data
        #KUNNR
        self.send_value("tkkunden", "subc1_tkkunnr")
        #FKTO
        self.send_value("fkto_vert", "subc1_fkto")
        #OK
        self.send_value("ok_vert", "subc1_ok")
        #Suche
        self.click_and_go(self.get_elemet("subc1_vk_suchen", how=By.ID), "Button:Kuendigung->Suchen")
        #ALNR
        self.send_value("anlalnr", "subc1_alnr")
        #Date 1
        self.send_value("vuntdat", "subc1_untdt")
        #LAUFK
        self.select_value("laufk", "subc1_laufk")
        #Date 2
        self.send_value("instterm", "subc1_dtmae")
        #Text
        self.get_elemet("subc1_vertragspartner_tray-exp").click()
        self.send_value("vertrtext", "subc1_vpzb")
        #not necessarily
        self.send_value("instbeg", "subc1_insb", how=By.ID, not_req="X")
        self.send_value("begprspf", "subc1_begprf", how=By.ID, not_req="X")
        self.send_value("vertbez", "subc1_vertragsbeziehungen", how=By.ID, not_req="X")
        if self.select_radiobutton("nbab", "subc1_nebenabreden_tray-exp", not_req="X"):
            self.send_value("nbab", "subc1_nbab", how=By.ID, not_req="X")
        if self.select_radiobutton("bmkg", "subc1_bemerkungen_tray-exp", not_req="X"):
            self.send_value("bmkg", "subc1_bmkg", how=By.ID, not_req="X")  
        self.send_value("berater", "subc1_berater", how=By.ID, not_req="X")
        #Save auftrag
        self.click_and_go(self.get_elemet("subc1_btn_auftrag_save", how=By.ID), "Tab: Miete->anlegen->Save auftrag")
        self.logger.info("Miete auftrag angelegt - Status 100")
        #Netzplanv.nr.
        self.send_value("npvnr", "subc1_rkauf")
        #Next tab
        self.click_and_go(self.get_elemet("subc1_aanlegen-itm-1-txt", how=By.ID), "Tab: Miete->anlegen->Systemubersicht")
        i = 1
        while True:
            if not self.send_value("posmenge_"+str(i), "subc1_mdlauftrag_positionen[{number}].menge".format(number=i), how=By.ID, not_req="X"):
                break
            if not self.send_value("posalnr_"+str(i), "subc1_mdlauftrag_positionen[{number}].alnr".format(number=i), how=By.ID, not_req="X"):
                break
            #Chek Prise
            self.click_and_go(self.get_elemet("subc1_pos_pruefen", how=By.ID), "Tab: Miete->Position->Pruefe", l_noerror=True)
            if not self.send_value("posprice_"+str(i), "subc1_mdlauftrag_positionen[{number}].nettopreis".format(number=i), not_req="X"): 
                break
            self.send_value("postext_"+str(i), "subc1_mdlauftrag_positionen[{number}].text".format(number=i), how=By.ID, not_req="X")
            self.select_value("servlev_"+str(i), "subc1_mdlauftrag_positionen[{number}].servlev".format(number=i), how=By.ID, not_req="X")
            i += 1
        #Save items
        self.click_and_go(self.get_elemet("subc1_btn_auftrag_save", how=By.ID), "Tab: Miete->Position->Save auftrag", is_alert="X")
        #Save docs
        self.click_and_go(self.get_elemet("subc1_aanlegen-itm-2-txt", how=By.ID), "Tab: Miete->kommerziellen Bedingungen")
        #not necessarily
        self.send_value("vrtsummemiet", "subc1_miete_vertrag", how=By.ID, not_req="X")
        self.send_value("projrabatt", "subc1_proj_rabatt", how=By.ID, not_req="X")
        self.send_value("einmalabr", "subc1_kozu_einmal_abr", how=By.ID, not_req="X")
        #Sava (Akkt)
        self.click_and_go(self.get_elemet("subc1_aktualisieren_kommerzbed", how=By.ID), "Button: Miete->kommerziellen Bedingungen->Aktualisieren")
        #Next tab (Installationskosten)
        self.click_and_go(self.get_elemet("subc1_aanlegen-itm-3-txt", how=By.ID), "Tab: Miete->Installationskosten")
        #not necessarily
        self.select_value("tk_kz", "subc1_tk_kz", how=By.ID, not_req="X")
        self.select_value("ltg_kz", "subc1_ltg_kz", how=By.ID, not_req="X")
        self.select_value("fv_kz", "subc1_fv_kz", how=By.ID, not_req="X")
        #data
        self.send_value("tkvereinblaut", "subc1_tkanlvereinblautvertr", how=By.ID, not_req="X")
        self.send_value("ltgvereinblaut", "subc1_ltgnvereinblautvertr", how=By.ID, not_req="X")
        self.send_value("fvvereinblaut", "subc1_fvvereinblautvertr", how=By.ID, not_req="X")
        #Save
        self.click_and_go(self.get_elemet("subc1_btn_auftrag_save", how=By.ID), "Button: Miete->Installationskosten->save")
        #Next tab (Dokumente / MtA)
        self.click_and_go(self.get_elemet("subc1_aanlegen-itm-4-txt", how=By.ID), "Tab: Miete->Installationskosten->Dokumente / MtA")
        #not necessarily
        if self.select_radiobutton("kundansch_tch", "subc1_kundenanschrift-exp", how=By.ID, not_req="X"):
            self.send_value("ansch_anrede", "subc1_anred", how=By.ID, not_req="X")
            self.send_value("ansch_name", "subc1_name1", how=By.ID, not_req="X")
            # self.send_value("ansch_name2", "subc1_name2", how=By.ID, not_req="X")
            self.send_value("ansch_vorname", "subc1_name3", how=By.ID, not_req="X")
            self.send_value("ansch_strasse", "subc1_stras", how=By.ID, not_req="X")
            self.send_value("ansch_hausnummer", "subc1_hsnmr", how=By.ID, not_req="X")
            self.send_value("ansch_hauszus", "subc1_hsnmrzus", how=By.ID, not_req="X")
            # self.send_value("ansch_postleitzahl", "subc1_pstlz", how=By.ID, not_req="X")
            # self.send_value("ansch_ort", "subc1_ort", how=By.ID, not_req="X")
        #Next!!! Textbausteine Kundenanschreiben
        if self.select_radiobutton("kundanschtxt_tch", "subc1_kundenanschreiben-exp", how=By.ID, not_req="X"):    #Kundenanschreiben
            self.send_value("anschtxt_uzei", "subc1_uzei", how=By.ID, not_req="X")
            self.send_value("anschtxt_duwa", "subc1_duwa", how=By.ID, not_req="X")
            self.send_value("anschtxt_betr", "subc1_betr", how=By.ID, not_req="X")
            self.send_value("anschtxt_betr2", "subc1_betr2", how=By.ID, not_req="X")
            self.send_value("anschtxt_betr3", "subc1_betr3", how=By.ID, not_req="X")
            self.send_value("anschtxt_zaft", "subc1_zaft", how=By.ID, not_req="X")
        #Next!!!Unterschriftsregelung
        if self.get_param("unterschrift_tch", "X"):
            self.click_and_go(self.get_elemet("subc1_unterschriftsregelung-exp", how=By.ID), "Unterschriftsregelung")
            self.select_radiobutton("unterschrift_fnk_1", "subc1_mdlauftrag_kdanschr.zart__subc1_zart_btn1-img", how=By.ID, not_req="X")
            self.select_radiobutton("unterschrift_fnk_2", "subc1_mdlauftrag_kdanschr.zart__subc1_zart_btn2-img", how=By.ID, not_req="X")
            self.select_radiobutton("unterschrift_fnk_3", "subc1_mdlauftrag_kdanschr.zart__subc1_zart_btn3-img", how=By.ID, not_req="X")
            self.select_radiobutton("unterschrift_opt11", "subc1_mdlauftrag_kdanschr.usrg1__subc1_usrg1_btn1-img", how=By.ID, not_req="X")
            self.select_radiobutton("unterschrift_opt12", "subc1_mdlauftrag_kdanschr.usrg1__subc1_usrg1_btn2-img", how=By.ID, not_req="X")
            self.select_radiobutton("unterschrift_opt13", "subc1_mdlauftrag_kdanschr.usrg1__subc1_usrg1_btn3-img", how=By.ID, not_req="X")
            self.select_radiobutton("unterschrift_opt14", "subc1_mdlauftrag_kdanschr.usrg1__subc1_usrg1_btn4-img", how=By.ID, not_req="X")
            #Data
            self.send_value("unterschrift_unte1", "subc1_unte1", how=By.ID, not_req="X")
            #21
            self.select_radiobutton("unterschrift_opt21", "subc1_mdlauftrag_kdanschr.usrg2__subc1_usrg2_btn1-img", how=By.ID, not_req="X")
            self.select_radiobutton("unterschrift_opt22", "subc1_mdlauftrag_kdanschr.usrg2__subc1_usrg2_btn2-img", how=By.ID, not_req="X")
            self.select_radiobutton("unterschrift_opt23", "subc1_mdlauftrag_kdanschr.usrg2__subc1_usrg2_btn3-img", how=By.ID, not_req="X")
            self.select_radiobutton("unterschrift_opt24", "subc1_mdlauftrag_kdanschr.usrg2__subc1_usrg2_btn4-img", how=By.ID, not_req="X")
            #Data
            self.send_value("unterschrift_unte2", "subc1_unte2", how=By.ID, not_req="X")
        #next!!! generierter Textblock
        if self.get_param("txtblock_tch", "X"):    #Kundenanschrift
            self.click_and_go(self.get_elemet("subc1_gentextblock-exp", how=By.ID), "generierter Textblock")
            self.send_value("txtblock_zvbp", "subc1_zvbp", how=By.ID, not_req="X")
            self.select_radiobutton("txtblock_save", "subc1_gen_kuend_schreiben", how=By.ID, not_req="X")
            self.select_radiobutton("txtblock_del", "subc1_gen_kuend_delete", how=By.ID, not_req="X")
        #Save
        self.click_and_go(self.get_elemet("subc1_btn_auftrag_save", how=By.ID), "Button:Nachtrag->Dokumente/MtA->save")
        #Montage-/Serviceauftrag
        self.click_and_go(self.get_elemet("subc1_mdlauftrag_kdanschr.radiogrp1__subc1_btn2-img", how=By.ID), "Button:Nachtrag->Montage-/Serviceauftrag")
        #not necessarily
        if self.select_radiobutton("hinweis_serv", "subc1_hinwServ-exp", not_req="X"):
            self.send_value("hinweis_serv", "subc1_hwsc", how=By.ID, not_req="X")
        if self.select_radiobutton("hinweis_fertig", "subc1_hinweisfertigung_tray-exp", not_req="X"):
            self.send_value("hinweis_fertig", "subc1_hinweisfertigung", how=By.ID, not_req="X")
        #Sent mail
        self.select_radiobutton("montfrei_mail_need", "subc1_dok_mail", not_req="X", log_text="Button:Nachtrag->Montage-/Serviceauftrag->Mail")
        self.send_value("mont_rufnr", "subc1_beschrufnr", how=By.ID, not_req="X")
        #Montageauftrag freigabe -> status 810
        self.click_and_go(self.get_elemet("subc1_freigabe"), "Button:Nachtrag->Montage-/Serviceauftrag->Freigabe")
        self.logger.info("Status 810 gesetzt, Montageauftrag angelegt")
        #Montage auftrag
        #Go to Real. auftrag
        self.click_and_go(self.get_elemet("subc1_RealisierungsAuftrag"), "Button: Real. auftrag")
        #Data
        self.send_value("mntinstdt", "subc1_m_insdt")
        if self.select_radiobutton("mnthinweisfert", "subc1_hinweisfertigung_tray-exp", not_req="X"):
            self.send_value("mnthinweisfert", "subc1_hinweisfertigung", how=By.ID, not_req="X")
        self.select_value("mntabrkenn", "subc1_abrkenn", how=By.ID)
        #not necessarily
        self.send_value("mntbeschrufnr", "subc1_beschrufnr", how=By.ID, not_req="X")
        if self.select_radiobutton("mnthinweisabr", "subc1_hinweisabrechnung_tray-exp", not_req="X"):
            self.send_value("mnthinweisabr", "subc1_hinweisabrechnung", how=By.ID, not_req="X")
        if self.select_radiobutton("mnthinweisserv", "subc1_hinwServ-exp", not_req="X"):
            self.send_value("mnthinweisserv", "subc1_hwsc", how=By.ID, not_req="X")
        #ABBV Text
        #Aufrtagpos text
        if self.get_param("mntabbvpos_tch", "X"):
            self.get_elemet("(//a[@id='subc1_abbv_txt_pflegen']/span)[1]", how=By.XPATH).click()
            #Get type
            self.select_radiobutton("mntabbvpos_clk_1", "subc1_mdlauftrag_abbv_txt.radiotext__subc1_kuntext_einmalig-img", how=By.ID, not_req="X")
            self.select_radiobutton("mntabbvpos_clk_2", "subc1_mdlauftrag_abbv_txt.radiotext__subc1_kuntext_wiederkehrend-img", how=By.ID, not_req="X")
            self.select_radiobutton("mntabbvpos_clk_3", "subc1_mdlauftrag_abbv_txt.radiotext__subc1_kuntext_intervall-img", how=By.ID, not_req="X")
            i = 1
            while True:
                if not self.send_value("mntabbvpos_txt_"+str(i), "subc1_vertragtext_ver"+str(i), how=By.ID, not_req="X"):
                    break 
                i += 1
            self.send_value("mntabbvpos_int_anz", "subc1_kuntext_intervall_anz", how=By.ID, not_req="X")
            #Close
            self.click_and_go(self.get_elemet("subc1_close", how=By.ID), "Button: ABBV-Text close")
        #Auftrag text
        if self.get_param("mntabbvauf_tch", "X"):
            self.get_elemet("(//a[@id='subc1_abbv_txt_pflegen']/span)[2]", how=By.XPATH).click()
            #Get type
            self.select_radiobutton("mntabbvpos_clk_1", "subc1_mdlauftrag_abbv_txt.radiotext__subc1_kuntext_einmalig-img", how=By.ID, not_req="X")
            self.select_radiobutton("mntabbvpos_clk_2", "subc1_mdlauftrag_abbv_txt.radiotext__subc1_kuntext_wiederkehrend-img", how=By.ID, not_req="X")
            self.select_radiobutton("mntabbvpos_clk_3", "subc1_mdlauftrag_abbv_txt.radiotext__subc1_kuntext_intervall-img", how=By.ID, not_req="X")
            i = 1
            while True:
                if not self.send_value("mntabbvpos_txt_"+str(i), "subc1_vertragtext_ver"+str(i), how=By.ID, not_req="X"):
                    break
                i += 1
            self.send_value("mntabbvpos_int_anz", "subc1_kuntext_intervall_anz", how=By.ID, not_req="X")
            #Close
            self.click_and_go(self.get_elemet("subc1_close", how=By.ID), "Button: ABBV-Text close")
        #Go to Position
        self.click_and_go(self.get_elemet("subc1_rauf-itm-1-txt", how=By.ID), "Tab: Position")
        #TODO: Preise and Erledigt date check
        i = 1
        while True:
            if not self.send_value("mntposmenge_"+str(i), "subc1_mdlauftrag_positionen[{number}].menge".format(number=i), how=By.ID, not_req="X"):
                break
            if not self.send_value("mntposalnr_"+str(i), "subc1_mdlauftrag_positionen[{number}].alnr".format(number=i), how=By.ID, not_req="X"):
                break
            #Chek Prise
            self.click_and_go(self.get_elemet("subc1_pos_pruefen", how=By.ID), "Tab: Miete->Position->Pruefe", l_noerror=True)
            if not self.send_value("mntposprice_"+str(i), "subc1_mdlauftrag_positionen[{number}].nettopreis".format(number=i), not_req="X"): 
                break
            self.send_value("mntpostext_"+str(i), "subc1_mdlauftrag_positionen[{number}].text".format(number=i), how=By.ID, not_req="X")
            self.select_value("mntservlev_"+str(i), "subc1_mdlauftrag_positionen[{number}].servlev".format(number=i), how=By.ID, not_req="X")
            i += 1
        #save
        self.click_and_go(self.get_elemet("subc1_save_rauftrag", how=By.ID), "Tab: Position->Save")
        #Go to Montagekosten
        self.click_and_go(self.get_elemet("subc1_rauf-itm-2-txt", how=By.ID), "Tab: Montagekosten")
        i = 1
        while True:
            if not self.send_value("mntmenge_"+str(i), "subc1_mdlauftrag_ref_tab_montkost[{number}].menge".format(number=i), how=By.ID, not_req="X"):
                break
            if not self.send_value("mntalnr_"+str(i), "subc1_mdlauftrag_ref_tab_montkost[{number}].alnr".format(number=i), how=By.ID, not_req="X"):
                break
            self.click_and_go(self.get_elemet("subc1_pos_pruefen", how=By.ID), "Tab: Montagekosten->Pruefe", l_noerror=True)
            #Price - inactive - no error
            self.send_value("mntprice_"+str(i), "subc1_mdlauftrag_ref_tab_montkost[1].lpreis".format(number=i), not_req="X")
            self.send_value("mnttxt_"+str(i), "subc1_mdlauftrag_ref_tab_montkost[{number}].text".format(number=i), how=By.ID, not_req="X")
            i += 1
        self.click_and_go(self.get_elemet("subc1_save_rauftrag", how=By.ID), "Button: Montagekosten->Save")
        #Go to Installationskosten
        self.click_and_go(self.get_elemet("subc1_rauf-itm-3-txt", how=By.ID), "Tab: Installationskosten")
        self.select_value("mnttk_kz", "subc1_tk_kz", how=By.ID, not_req="X")
        self.select_value("mntltg_kz", "subc1_ltg_kz", how=By.ID, not_req="X")
        self.select_value("mntfv_kz", "subc1_fv_kz", how=By.ID, not_req="X")
        #data
        self.send_value("mnttkvereinblaut", "subc1_tkanlvereinblautvertr", how=By.ID, not_req="X")
        self.send_value("mntltgvereinblaut", "subc1_ltgnvereinblautvertr", how=By.ID, not_req="X")
        self.send_value("mntfvvereinblaut", "subc1_fvvereinblautvertr", how=By.ID, not_req="X")
        #Save
        self.click_and_go(self.get_elemet("subc1_save_rauftrag", how=By.ID), "Button: Installationskosten->Save")
        #Go to kommerzielle Bedingungen
        self.click_and_go(self.get_elemet("subc1_rauf-itm-4-txt", how=By.ID), "Tab: kommerzielle Bedingungen")
        #not
        self.send_value("mntvrtsummemiet", "subc1_miete_vertrag", how=By.ID, not_req="X")
        self.send_value("mntprojrabatt", "subc1_proj_rabatt", how=By.ID, not_req="X")
        self.send_value("mnteinmalabr", "subc1_kozu_einmal_abr", how=By.ID, not_req="X")
        #Aktualisieren
        self.click_and_go(self.get_elemet("subc1_aktualisieren_kommerzbed", how=By.ID), "Button: Aktualisieren")
        #Save
        self.click_and_go(self.get_elemet("subc1_save_rauftrag", how=By.ID), "Button: Installationskosten->Save")
        #Go to back TAB Grunddaten
        self.click_and_go(self.get_elemet("subc1_rauf-itm-0-txt", how=By.ID), "Tab: TAB Grunddaten")
        #Close mobntage auftrag Status 820
        self.select_radiobutton("mntaufgeschl", "subc1_auftrabgeschl-img", how=By.ID, not_req="X")
        self.click_and_go(self.get_elemet("subc1_save_rauftrag", how=By.ID), "Button: TAB Grunddaten->Save")
        if abrkenn == '11':
            self.check_div_popup(popup_ids="popup_faktura", button_class="urBtnStd urV", button_ids="subc1_fakt_abbv_ok")
            self.logger.info("Auftrag abgeschlossen - Status 990")
        else:
            self.check_div_popup(popup_ids="ok_popup", button_class="urBtnStd urV", button_ids="subc1_install_ok")
            self.logger.info("Montageauftrag erledigt - Status 820")
            #Go to Verrechnungsauftrag
            self.click_and_go(self.get_elemet("subc1_tr_realaufverr", how=By.ID), "Menu: Verrechnungsauftrag")
            #Navi
            self.popup_navigation(0, "MENU_VERR_DUMY_E", log_text="Menu:Verrechnungsauftrag->aendern")
            #go to tab Datenuebergabe Abrechnung
            self.click_and_go(self.get_elemet("subc1_rauf-itm-1-txt", how=By.ID), "Tab: Verrechnungsauftrag->Datenuebergabe Abrechnung")
            self.click_and_go(self.get_elemet("subc1_dok_anz", how=By.ID), "Tab: Verrechnungsauftrag->Mail an Anwender")
            self.click_and_go(self.get_elemet("subc1_erled", how=By.ID), "Tab: Verrechnungsauftrag->Datenuebergabe starten")
            self.logger.info("Auftrag abgeschlossen - Status 990")
        #Done
        self.resp_vars['vertrnr'] = self.get_elemet("subc1_vertrg").get_attribute("value")
        self.resp_vars['tkkunden'] = self.get_elemet("subc1_tkkunnr").get_attribute("value")
        self.resp_vars['ok'] = self.get_elemet("stdot").get_attribute("value")
        self.resp_vars['veraufnr'] = self.get_elemet("subc1_aufnr").get_attribute("value")