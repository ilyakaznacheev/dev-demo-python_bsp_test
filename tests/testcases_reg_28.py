from selenium import webdriver
from selenium.webdriver.common.by import By
from lib import testcase_basic

class Testcases_reg_28(testcase_basic.PrimaryTest):
    """
    Regretion test 28
    Andrey Danilin
    T-systems RUS
    2015
    Lege FKTO-Wechsel zu alle Vertraege an
    """
    def runTest(self):
        #BLOCK: Open
        driver = self.driver
        driver.get(self.base_url)
        #BLOCK: Logon
        self.logon()
        #BLOCK: Data add
        self.click_and_go(self.get_elemet("subc1_Kundendaten"), "Menu: Kundendaten")
        self.click_and_go(self.get_elemet("subc1_fktowechsel"), "Button: Kundendaten->FKTO_Wechsel")
        #Data
        self.send_value("tkkunden_wech", "subc1_ywvko")
        self.send_value("fkto_wech", "subc1_fkto")
        #Suche
        self.click_and_go(self.get_elemet("subc1_kusu2"), "Menu: FKTO_Wechsel->Suche")
        #Data
        self.send_value("fkto_wech_neu", "subc1_fktoneu")
        self.send_value("datum_neu", "subc1_wirkszum")
        self.click_and_go(self.get_elemet("subc1_fktowechselallevertraege"), "Button: Uebernahme fuer alle Vertraege")
        text = self.click_and_go(self.get_elemet("subc1_b_fktowechsel"), "Button: Speichern", is_alert="X")
        if text != self.get_param("msg_text"):
            raise Exception
        #Done
        self.resp_vars['fkto_wech'] = self.get_param("fkto_wech")
        self.resp_vars['fkto_wech_neu'] = self.get_param("fkto_wech_neu")
        #Alle Auftraege
        j = 1
        l_flag = True
        while l_flag:
            i = 2
            while i<10:
                try:
                    l_vertrnr = self.get_elemet("//table[@id='subc1_fktowechselvertraege']/tbody/tr/td/table/tbody/tr[{}]/td[2]/div/span".format(str(i)), how=By.XPATH).text
                    l_old_fkro = self.get_elemet("//table[@id='subc1_fktowechselvertraege']/tbody/tr/td/table/tbody/tr[{}]/td[5]/div/span".format(str(i)), how=By.XPATH).text
                except:
                    break
                if l_vertrnr <> ' ' and l_old_fkro <> ' ':
                    self.resp_vars['fkto_wech_vertrnr_'+str(j)] = l_vertrnr
                    self.resp_vars['fkto_wech_vn_fkto_'+str(j)] = l_old_fkro
                else:
                    break
                j += 1
                i += 1
            page = self.get_elemet("subc1_fktowechselvertraege_pager-inp").get_attribute("value")
            self.click_and_go(self.get_elemet("subc1_fktowechselvertraege_pager-btn-4"), "Next page")
            if page>=self.get_elemet("subc1_fktowechselvertraege_pager-inp").get_attribute("value"):
                l_flag = False
