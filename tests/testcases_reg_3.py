from selenium import webdriver
from selenium.webdriver.common.by import By
from lib import testcase_basic

class Testcases_reg_3(testcase_basic.PrimaryTest):
    """
    Regretion test 3
    Andrey Danilin
    T-systems RUS
    2015
    Regulierer mit neuer FKTO anlegen. SEPA Bankverbindung dazu erfassen.
    """
    def runTest(self):
        #BLOCK: Open
        driver = self.driver
        driver.get(self.base_url)
        #BLOCK: Logon
        self.logon()
        #BLOCK: Navi
        self.click_and_go(self.get_elemet("subc1_Kundendaten"), "Menu:Kundendaten")
        self.click_and_go(self.get_elemet("subc1_tr_krech"), "Menu:Regulierer")
        #Unsupported command [selectWindow | name=sapPopupMainId_X2 | ]
        self.popup_navigation(0, "MENU_REGULIERERBEARB_N", log_text="Menu:Anlegen")
        #BLOCK: Data add
        self.send_value("tkkunden", "subc1_ywvko")
        self.click_and_go(self.get_elemet("subc1_kusu2"), "Button: Kunden Suchen")
        #Data
        self.select_value("tkkanrede_reg", "subc1_anred")
        self.send_value("tkkname_reg", "subc1_name_1")
        self.send_value("tkkstrass_reg", "subc1_yygstras01")
        self.send_value("tkkpstlz_reg", "subc1_pstl1")
        self.send_value("tkkort_reg", "subc1_ort01")
        self.send_value("tkkhausnr_reg", "subc1_yyghausnr")
        self.select_value("tkkustkz", "subc1_ustkz")
        self.send_value("fkto_neu", "subc1_fkto")
        self.send_value("tkkelfe", "subc1_elfe")
        #Next Tab
        self.click_and_go(self.get_elemet("subc1_arechanschr-itm-1-txt"), "Tab: Bankverbindung aendern")
        #Data
        self.select_value("tkkzahlweg", "subc1_zahlweg", not_req="X")
        self.send_value("sepabic", "subc1_bic", how=By.ID, not_req="X")
        self.send_value("sepaiban", "subc1_iban", how=By.ID, not_req="X")
        self.send_value("sepamandnr", "subc1_mandref", how=By.ID, not_req="X")
        self.send_value("sepakontoin", "subc1_koinh", how=By.ID, not_req="X")
        #Save Data
        #TODO: del try
        self.click_and_go(self.get_elemet("subc1_reganl"), "Button: Speichern") #Test: no alert
        self.click_and_go(self.get_elemet("subc1_arechanschr-itm-0-txt"), "Tab: Rechnunganschrift")
        #Done
        self.resp_vars['tc_kunnr_v'] = self.get_elemet("subc1_ywvko").get_attribute("value")
        self.resp_vars['tc_fkto_v'] = self.get_elemet("subc1_fkto").get_attribute("value")
        self.resp_vars['tkkklsid'] = self.get_elemet("subc1_klsid").get_attribute("value")