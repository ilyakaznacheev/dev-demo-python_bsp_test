from selenium.webdriver.support.ui import Select
import time
from lib import testcase_basic

class Testcases_reg_1(testcase_basic.PrimaryTest):
    """
    Regretion test 1
    Andrey Danilin
    T-systems RUS
    2015
    Change user's email address
    Benutzerdaten aendern Aendere Email-Adresse
    """
    def runTest(self):
        #BLOCK: Open
        driver = self.driver
        driver.get(self.base_url)
        #BLOCK: Logon 
        self.logon()
        #BLOCK: Data add
        self.click_and_go(self.get_elemet("subc1_Benutzerpflege"), "Menu: Benutzerpflege")
        self.click_and_go(self.get_elemet("subc1_useraendern"), "Button: Benutzerpflege->Userandern")
        self.send_value("useremail", "subc1_e_mail")
        self.click_and_go(self.get_elemet("subc1_useraendern"), "Button: Benutzerpflege->Speichern")
        self.resp_vars['useremail'] = self.get_elemet("subc1_e_mail").get_attribute("value")