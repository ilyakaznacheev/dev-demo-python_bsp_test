from selenium import webdriver
from selenium.webdriver.common.by import By
from lib import testcase_basic

class Testcases_reg_6(testcase_basic.PrimaryTest):
    """
    Regretion test 6
    Andrey Danilin
    T-systems RUS
    2015
    aendere Standort
    """
    def runTest(self):
        #BLOCK: Open
        driver = self.driver
        driver.get(self.base_url)
        #BLOCK: Logon
        self.logon()
        #BLOCK: Navi
        self.click_and_go(self.get_elemet("subc1_Kundendaten"), "Menu:Kundendaten")
        self.click_and_go(self.get_elemet("subc1_tr_standort"), "Menu:Standort")
        #Unsupported command [selectWindow | name=sapPopupMainId_X2 | ]
        self.popup_navigation(0, "MENU_STANDORTBEARB_E", log_text="Menu:Standortaendern")
        #BLOCK: Data add
        self.send_value("tkkunden", "subc1_ywvko")
        self.send_value("ok", "subc1_ok")
        self.click_and_go(self.get_elemet("subc1_kusu2"), "Button: Kunden und ok Suchen")
        #Data
        # self.send_value("tkkhausnr_ort", "subc1_yyghausnr")
        #not necessarily
        self.send_value("tkkname_ort", "subc1_name1", how=By.ID, not_req="X")
        # self.send_value("tkkstrass_ort", "subc1_yygstras01", how=By.ID, not_req="X")
        # self.send_value("tkkpstlz_ort", "subc1_pstlz1", how=By.ID, not_req="X")
        # self.send_value("tkkort_ort", "subc1_ort01", how=By.ID, not_req="X")
        self.send_value("tkkname_2_ort", "subc1_name2", how=By.ID, not_req="X")
        # self.send_value("tkkhausnr_2_ort", "subc1_yyghausnrz", how=By.ID, not_req="X")
        self.send_value("stdgebaud", "subc1_yyggebtl", how=By.ID, not_req="X")
        self.send_value("stdgebaudzu", "subc1_YYGGEBTLZU", how=By.ID, not_req="X")
        self.send_value("stdfax", "subc1_TELFX", how=By.ID, not_req="X")
        #Save Data
        self.click_and_go(self.get_elemet("subc1_standortsave"), "Button: Speichern")
        #Output data
        self.resp_vars['tkkunden'] = self.get_elemet("subc1_ywvko").get_attribute("value")
        self.resp_vars['ok'] = self.get_elemet("subc1_ok1").get_attribute("value")
        self.resp_vars['tkkklsid'] = self.get_elemet("subc1_klsid1").get_attribute("value")