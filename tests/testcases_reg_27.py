from selenium import webdriver
from selenium.webdriver.common.by import By
from lib import testcase_basic

class Testcases_reg_27(testcase_basic.PrimaryTest):
    """
    Regretion test 27
    Andrey Danilin
    T-systems RUS
    2015
    Lege FKTO-Wechsel zu einem Vertrag an
    """
    def runTest(self):
        #BLOCK: Open
        driver = self.driver
        driver.get(self.base_url)
        #BLOCK: Logon
        self.logon()
        #BLOCK: Data add
        self.click_and_go(self.get_elemet("subc1_Kundendaten"), "Menu: Kundendaten")
        self.click_and_go(self.get_elemet("subc1_fktowechsel"), "Button: Kundendaten->FKTO_Wechsel")
        #Data
        self.send_value("tkkunden_wech", "subc1_ywvko")
        self.send_value("fkto_wech", "subc1_fkto")
        #Suche
        self.click_and_go(self.get_elemet("subc1_kusu2"), "Menu: FKTO_Wechsel->Suche")
        #Data
        flag = 0
        inp_nr = self.get_param("vertrnr_wech")
        page_nr = 1
        while True:
            i = 2
            while i<10:
                web_nr = self.get_elemet("//table[@id='subc1_fktowechselvertraege']/tbody/tr/td/table/tbody/tr[{}]/td[2]/div/span".format(str(i)), how=By.XPATH).text
                if web_nr==inp_nr: 
                    self.send_value("fkto_wech_neu", "subc1_mdlkunde_fktowechsel_tab[{}].fkto".format(str(8 * (page_nr - 1) + i - 1)))
                    self.send_value("datum_neu", "subc1_mdlkunde_fktowechsel_tab[{}].datum".format(str(8 * (page_nr - 1) + i - 1)))
                    l_vertrnr = web_nr
                    l_fkro_web = self.get_elemet("//table[@id='subc1_fktowechselvertraege']/tbody/tr/td/table/tbody/tr[{}]/td[5]/div/span".format(str(i)), how=By.XPATH).text
                    flag = 1
                    break
                i += 1
            if flag==1:
                break
            else:
                page_nr += 1
                page = self.get_elemet("subc1_fktowechselvertraege_pager-inp").get_attribute("value")
                self.click_and_go(self.get_elemet("subc1_fktowechselvertraege_pager-btn-4"), "Next page")
                if page==self.get_elemet("subc1_fktowechselvertraege_pager-inp").get_attribute("value"):
                    self.logger.error("Vertragnr not found!")
                    raise Exception
        text = self.click_and_go(self.get_elemet("subc1_b_fktowechsel"), "Button: Speichern", is_alert="X")
        if text != self.get_param("msg_text"):
            raise Exception
        #Done
        self.resp_vars['fkto_wech'] = self.get_param("fkto_wech")
        self.resp_vars['fkto_wech_neu'] = self.get_param("fkto_wech_neu")
        self.resp_vars['fkto_wech_vertrnr_1'] = l_vertrnr
        self.resp_vars['fkto_wech_vn_fkto_1'] = l_fkro_web
        