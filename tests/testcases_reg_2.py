from selenium import webdriver
from selenium.webdriver.common.by import By
from lib import testcase_basic

class Testcases_reg_2(testcase_basic.PrimaryTest):
    """
    Regretion test 2
    Andrey Danilin
    T-systems RUS
    2015
    Kundensuche ueber KKS
    """
    def runTest(self):
        #BLOCK: Open
        driver = self.driver
        driver.get(self.base_url)
        #BLOCK: Logon
        self.logon()
        #BLOCK: Navi
        self.click_and_go(self.get_elemet("subc1_Kundendaten"), "Menu:Kundendaten")
        #Suche
        self.send_value("tkkname", "subc1_name")
        self.send_value("tkkort", "subc1_ort")
        self.send_value("tkkpstlz", "subc1_pstlz")
        self.click_and_go(self.get_elemet("subc1_kusu1"), "Button: Kundensuchen")
        #Work with table
        table_element = self.get_elemet("table.urSAPTable", By.CSS_SELECTOR)
        lines = table_element.find_elements_by_tag_name("tr")
        try:
            item_data = lines[0].find_element_by_css_selector("span.urTxtStd.urVt1")
            self.resp_vars['tkkunden'] = item_data.text
        except:
            self.logger.error("Kein Kunde gefunden!")