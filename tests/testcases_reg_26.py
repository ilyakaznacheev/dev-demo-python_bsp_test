from selenium import webdriver
from selenium.webdriver.common.by import By
from lib import testcase_basic

class Testcases_reg_26(testcase_basic.PrimaryTest):
    """
    Regretion test 26
    Andrey Danilin
    T-systems RUS
    2015
    Lege URUP zwischen zwei Kunden an
    """
    def runTest(self):
        #BLOCK: Open
        driver = self.driver
        driver.get(self.base_url)
        #BLOCK: Logon
        self.logon()
        #BLOCK: Data add
        self.click_and_go(self.get_elemet("subc1_Kundendaten"), "Menu: Kundendaten")
        self.click_and_go(self.get_elemet("subc1_uerup"), "Button: Kundendaten->Uerup")
        #Data
        self.send_value("tkkunden_uerup", "subc1_tkkunnr")
        self.send_value("fkto_uerup_alt", "subc1_fkto", not_req='X')
        #Suche
        self.click_and_go(self.get_elemet("subc1_uerup_suche"), "Menu: URUP->Suche")
        #Data
        self.send_value("tkkunden_uerup_neu", "subc1_tkkunnr_neu")
        self.click_and_go(self.get_elemet("subc1_kusu4"), "Menu: URUP->Suche neu Kunden")
        #DAta
        self.send_value("vertrnr_uerup", "subc1_mdlkunde_uerup.vnr_alt", how=By.NAME)
        self.send_value("fkto_uerup_neu", "subc1_fkto_uerup")
        #Not
        self.send_value("delkos", "subc1_delkos", how=By.ID, not_req="X")
        #date
        self.send_value("ueberdat_uerup", "subc1_uebertragungsdatum_neu")
        self.send_value("vertrdat_uerup", "subc1_vertragsdatum_neu")
        self.send_value("berater_uerup", "subc1_berater")
        #not (text)
        if self.select_radiobutton("zeich_txt", "subc1_zb_neu_tray-exp", how=By.ID, not_req="X", log_text="Zeichnungsberechtigter"):
            self.send_value("zeich_txt", "subc1_zeichnungsberechtigter_neu")
        if self.select_radiobutton("nbab_txt", "subc1_nebenabreden_tray-exp", how=By.ID, not_req="X", log_text="Nebenabreden"):
            self.send_value("nbab_txt", "subc1_nbab")
        if self.select_radiobutton("bemerk_txt", "subc1_bemerkungen_tray-exp", how=By.ID, not_req="X", log_text="Bemerkungen"):
            self.send_value("bemerk_txt", "subc1_bmkg")
        #Save
        self.click_and_go(self.get_elemet("subc1_uerup_zustimmen"), "Menu: URUP->Save")
        #Done
        self.resp_vars['vertrnr'] = self.get_elemet("subc1_vnr_alt").get_attribute("value")
        self.resp_vars['urup_id'] = self.get_elemet("subc1_uerup_id").get_attribute("value")
        self.resp_vars['vertrnr_neu'] = self.get_elemet("subc1_vnr_neu").get_attribute("value")
        