import ConfigParser


class ImprovedParser(object):
    """docstring for ImprovedParser"""
    def __init__(self, user_path, default_path):
        self.user_dict = self._read_sections(user_path)
        self.default_dict = self._read_sections(default_path)

    def _read_sections(self, user_path):
        config = ConfigParser.ConfigParser()
        config.read(user_path)

        sections = dict.fromkeys(config.sections())

        for key in sections.keys():
            sections[key] = dict(config.items(key))

        return sections

    def get_conf(self, section, key):
        local_section = self.user_dict.get(section, self.default_dict[section])
        value = local_section.get(key, self.default_dict[section][key])
        return value
