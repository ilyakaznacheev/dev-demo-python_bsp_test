import sys
import unittest
import ConfigParser
import os
import StringIO
import json
from lib import testcase_logger
from lib import config_parser
from tests import *


class Control(object):
    """ primary controller """
    USER_CONF_PATH = 'settings.conf'
    DEFAULT_CONF_PATH = 'lib/default.conf'
    DEFAULT_JSON_PATH = 'response.json'
    DEFAULT_XML_PATH = 'response.xml'

    RESULT_DESCRIPTION = {
        '.': "OK",
        'E': "ERROR",
        'F': "FAILED "
        }

    def __init__(self, json_path=None, xml_path=None):
        """ Constructor """

        if json_path:
            self.response_type = "json"
            self.test_var_data = self._read_from_json(json_path)
        elif xml_path:
            self.response_type = "xml"
            self.test_var_data = self._read_from_xml(xml_path)
        else:
            sys.exit("Error: No data loaded")

        self.config_keys = config_parser.ImprovedParser(
            self.USER_CONF_PATH,
            self.DEFAULT_CONF_PATH
            )

        testpath = os.path.join(
            os.getcwd(), self.config_keys.get_conf('SystemPath', 'tests_path')
            )

        sys.path.append(testpath)  # TODO: simplify this

    def read_config(self, file_name=DEFAULT_CONF_PATH):
        """ Generate dictionary by
            values from config file """

        config = ConfigParser.ConfigParser()
        config.read(file_name)

        sections = dict.fromkeys(config.sections())

        for key in sections.keys():
            sections[key] = dict(config.items(key))

        return sections

    def run_tests(self):
        # create local file instance to write status messages
        test_file = StringIO.StringIO()

        suite = unittest.TestSuite()
        loggers = dict()
        resp_vars = dict()

        for item in self.test_var_data["TESTLIST"]:
            try:
                test_class = getattr(sys.modules[__name__], item["CLASSNAME"])
            except AttributeError:
                print("Error: module {} not found in {}".format(
                    item["CLASSNAME"], sys.modules[__name__]
                    ))
                continue

            # get class object by name
            logger = testcase_logger.SimpleLogger()
            responced = dict()

            # add testcase in testsuite
            suite.addTest(test_class(
                item["TESTID"],
                variables=item["VARIABLES"],
                log_file=logger,
                responsed_vars=responced
                ))

            # collect logs and variable data
            loggers[item["TESTID"]] = logger
            resp_vars[item["TESTID"]] = responced

        unittest.TextTestRunner(test_file).run(suite)

        test_id_list = [item["TESTID"] for item
                        in self.test_var_data["TESTLIST"]]

        primary_status = self._get_common_log(test_file, test_id_list)

        common_data = self._build_response(
            primary_status,
            self.test_var_data["TESTLAUFID"],
            resp_vars,
            primary_status,
            loggers
            )

        return common_data

    def _get_common_log(self, file_instance, id_list):
        """get test statuses"""
        status_list = list(file_instance.getvalue().splitlines()[0])
        common_status = dict()

        for test_name, status in zip(id_list, status_list):
            common_status[test_name] = self.RESULT_DESCRIPTION[status]

        return common_status

    def _read_from_json(self, path):
        """decapsulate data from json file"""
        try:
            raw_json_file = open(path, 'r')
        except IOError:
            sys.exit('Error: could not open {}'.format(path))

        prepared_json_file = json.load(raw_json_file, 'ISO-8859-1')

        self._translate_json_list(prepared_json_file)

        return prepared_json_file

    def _write_to_json(self, data, path):
        """encapsulate data to json file"""
        if not path:
            path = self.DEFAULT_JSON_PATH
        self._check_directory(path)
        try:
            response = open(path, "w+")
        except IOError:
            sys.exit('Error: could not open {}'.format(path))

        raw_ident = self.config_keys.get_conf('SystemVars', 'json_indent')

        indent = int(raw_ident) if raw_ident.lower() != "none" else None

        json.dump(data, response, indent=indent, ensure_ascii=False)

    def _read_from_xml(self, path):
        """decapsulate data from xml file"""
        # not implemented yet
        pass

    def _write_to_xml(self, data, path=DEFAULT_XML_PATH):
        """encapsulate data to xml file"""
        # not implemented yet
        pass

    def _translate_json_list(self, prepared_json_file):
        """translate weird SAP json variables format
           to normal dictonary (if needed)"""
        for item in prepared_json_file["TESTLIST"]:
            if isinstance(item["VARIABLES"], list):
                variables = item["VARIABLES"]
                new_dict = dict()
                for var_dict in variables:
                    new_dict[var_dict["KEY"].lower()] = var_dict["VALUE"]

                item["VARIABLES"] = new_dict

    def _build_response(self, id_list, testid, variables,
                        common_log, partial_log):
        """create dictionary of all test data"""
        response = dict()
        response["testlaufid"] = testid
        response["testlist"] = list()

        for test_id in id_list:
            testcase = dict()
            testcase["testid"] = test_id
            testcase["status"] = common_log[test_id]
            testcase["log"] = partial_log[test_id]
            testcase["variables"] = self._translate_dict_to_key_value(
                variables[test_id])
            response["testlist"].append(testcase)

        return response

    def _check_directory(self, path):
        """check if directory exists"""
        dirpath = os.path.dirname(path)

        if not os.path.exists(os.path.abspath(dirpath)):
            os.makedirs(dirpath)

    def _translate_dict_to_key_value(self, responced):
        resp_vars = list()
        for key, value in responced.iteritems():
            line = dict()
            line["field"] = key
            line["value"] = value
            resp_vars.append(line)
        return resp_vars

    def return_data(self, data, **path):
        if self.response_type == "json":
            self._write_to_json(data, **path)
        elif self.response_type == "xml":
            self._write_to_xml(data, **path)


if __name__ == "__main__":
    cl = Control('test')