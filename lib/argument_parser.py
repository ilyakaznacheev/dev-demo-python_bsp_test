import argparse


class ArgParser(argparse.ArgumentParser):
    def __init__(self, *args, **kwargs):
        super(ArgParser, self).__init__(*args, **kwargs)
        self._initiate_params()

    def _initiate_params(self):
        """initiate default parameters"""
        self.add_argument(
            "-x", "--xml",
            dest="xml",
            help="path to xml variable list (absolute or relative)"
            )
        self.add_argument(
            "-j", "--json",
            dest="json",
            help="path to json variable list (absolute or relative)"
            )
        self.add_argument(
            "-r", "--response",
            dest="response",
            help="path to response file (absolute or relative)"
            )
