import datetime


class SimpleLogger(list):
    """ dictionady-based logger"""
    CRITICAL = 50
    FATAL = CRITICAL
    ERROR = 40
    WARNING = 30
    WARN = WARNING
    INFO = 20
    DEBUG = 10
    NOTSET = 0

    _level_names = {
        CRITICAL: 'CRITICAL',
        ERROR: 'ERROR',
        WARNING: 'WARNING',
        INFO: 'INFO',
        DEBUG: 'DEBUG',
        NOTSET: 'NOTSET'
    }

    SEPARATOR = ':'
    MESSAGE_PATTERN = u"{heading}{separator}{params}{msg_type}{separator}{msg_text}"
    TIMESTAMP_PATTERN = u"{year}-{month}-{day} {hour}:{minute}:{second}"

    def __init__(self, level=NOTSET):
        super(SimpleLogger, self).__init__()
        self.level = level

    def is_enabled_for(self, level):
        return level >= self.level

    def _log(self, msg_type, msg_text,
             heading=None, separator=SEPARATOR, additional=''
             ):

        if not heading:
            heading = self._get_timestamp()

        if additional:
            additional += self.SEPARATOR

        message = self.MESSAGE_PATTERN.format(
            heading=heading,
            params=additional,
            msg_type=msg_type,
            msg_text=msg_text,
            separator=separator
            )
        message = message.encode(encoding='UTF-8',errors='replace')
        self.append(message)

    def _get_timestamp(self):
        """generate timestamp"""
        current_time = datetime.datetime.now()
        timestamp = self.TIMESTAMP_PATTERN.format(
            year=current_time.year,
            month=current_time.month,
            day=current_time.day,
            hour=current_time.hour,
            minute=current_time.minute,
            second=current_time.second
            )
        return timestamp

    def debug(self, message):
        if self.is_enabled_for(self.DEBUG):
            self._log(self._level_names[self.DEBUG], message)

    def info(self, message):
        if self.is_enabled_for(self.INFO):
            self._log(self._level_names[self.INFO], message)

    def warn(self, message):
        if self.is_enabled_for(self.WARN):
            self._log(self._level_names[self.WARN], message)

    def warning(self, message):
        if self.is_enabled_for(self.WARNING):
            self._log(self._level_names[self.WARNING], message)

    def error(self, message):
        if self.is_enabled_for(self.ERROR):
            self._log(self._level_names[self.ERROR], message)

    def critical(self, message):
        if self.is_enabled_for(self.CRITICAL):
            self._log(self._level_names[self.CRITICAL], message)
