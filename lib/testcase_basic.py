from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from exceptions_web4all import UnexpectedAlert_web4al
import unittest
import time

class PrimaryTest(unittest.TestCase):

    DEFAULT_URL = "https://q4de8ncod69.bmbg.t-com.de:8445/sap/bc/bsp/sap/yw_web/session_single_frame.htm?sap-sessioncmd=open"
    
    def __init__(self, test_name, variables=dict(),
                 log_file=None, responsed_vars=dict()
                 ):
        super(PrimaryTest, self).__init__()
        self.test_name = test_name
        self.vars = variables
        self.logger = log_file
        self.resp_vars = responsed_vars
        self.main_window = None
        self.main_frame = None
        
    def setUp(self):
        try:
            self.driver = webdriver.Ie()
        except Exception, error:
            self.logger.critical("IE WebDriver error: " + str(error))
            raise error
        self.wait = WebDriverWait(self.driver, 10)

        self.driver.implicitly_wait(3)
        self.verificationErrors = []
        self.accept_next_alert = True

        self.base_url = self.get_param("urlweb4all")
        if not self.base_url:
            self.base_url = self.DEFAULT_URL

    def get_elemet(self, what, how=By.ID, no_log=None):
        try:
            return self.driver.find_element(by=how, value=what)
        except NoSuchElementException:
            if no_log != "X":
                self.logger.warning("Element \"{}\" not found!".format(what))
            raise NoSuchElementException
        
    def send_value(self, param, where, how=By.ID, not_req=None):
        param = self.get_param(param, not_req)
        if param:
            try:
                self.get_elemet(where, how=how).clear()
                self.get_elemet(where, how=how).send_keys(param)
            except:
                self.logger.warning("(Send_value)Element is inactive or invisible!".format(where))
                return False
            return True
        else:
            return False
            
    def select_value(self, param, where, how=By.ID, not_req=None):
        param = self.get_param(param, not_req)
        if param:
            try:
                Select(self.get_elemet(where, how=how)).select_by_visible_text(param)
                self.wait_until(how, where)
                return True
            except:
                self.logger.error("(Select)Element \"{}\" not found!".format(where))
                raise Exception
    
    def select_radiobutton(self, param, what, how=By.ID, not_req=None, log_text=None):
        if self.get_param(param, not_req):
            self.get_elemet(what, how=how).click()
            self.wait_until(how, what)
            if log_text: 
                if self.alert_check_and_logging():
                    self.logger.info(log_text)
            return True
        else:
            return False
    
    def check_new_window(self, is_error=False, close=None):
        for l_window in self.driver.window_handles:
            if l_window <> self.main_window:
                self.logger.info("Unexpected Window: {}".format(l_window))
                self.driver.switch_to.window(l_window)
                self.logger.info("Window Title = " + self.driver.title)
                if close:
                    self.driver.close()
                    self.logger.info("Window closed")
                self.driver.switch_to.window(self.main_window)
                self.driver.switch_to_frame(self.main_frame)
                if is_error:
                    self.logger.error("Error: Window is unexpected -> Error")
                break
            
    def check_div_popup(self, popup_ids="popup_faktura", button_class="urBtnStd urV", button_ids=None):
        l_result = False
        try:
            l_div_popup = self.driver.find_element_by_id(popup_ids)
        except NoSuchElementException:
            return l_result
            #try to find button
        l_result = True
        self.logger.info("PopUp was found! ID: {}".format(l_div_popup.get_attribute("ID")))
        try:
            if button_class and button_ids:
                l_btn = self.driver.find_element_by_xpath("//a[@class='{}' and @id='{}']".format(button_class,button_ids))
            elif button_class and not button_ids:
                l_btn = self.driver.find_element_by_xpath("//a[@class='{}']".format(button_class))
            else:
                return l_result
        except NoSuchElementException, error:
            self.logger.error("Button not found! Button: {} {}".format(button_class,button_ids))
            raise error 
        self.logger.info("Try to click! Button: "+l_btn.get_attribute('title'))
        self.click_and_go(l_btn, "Click in DIV-PopUp")
        
    def click_and_go(self, element, log_text, is_alert=None, l_noerror=None):
        element.click()
        l_text = self.alert_check_and_logging()
        if l_text:
            if not is_alert and not l_noerror:
                if log_text: self.logger.info("Trace: "+log_text)
                self.logger.error("Fatal Error: Unexpected Alert!")
                raise UnexpectedAlert_web4al
            else:
                return l_text
        else:
            if is_alert and not l_noerror:
                if log_text: self.logger.info("Trace: "+log_text)
                self.logger.warning("Alert not FOUND, but expected!->Try to continue")
            return None
            
        #Check other windows
        self.check_new_window(close=True)
        
    def popup_navigation(self, level, what, how=By.ID, log_text=None):
        iframe = self.get_elemet("sapPopupMainId_X"+str(level), how=By.ID)
        self.driver.switch_to_frame(iframe)
        try:
            l_first_elem = self.driver.find_element_by_xpath("//tr[@Idx='0']")
        except NoSuchElementException, error:
            self.logger.error("First Menu-element not found: "+"sapPopupMainId_X"+str(level))
            raise error
        ActionChains(self.driver).move_to_element(l_first_elem).perform()
        self.click_and_go(self.get_elemet(what, how=how),log_text)
        self.driver.switch_to_default_content()
        self.driver.switch_to_frame(self.main_frame)
        
    def alert_check_and_logging(self):
        #Check alert
        l_alert = EC.alert_is_present().__call__(self.driver)
        if l_alert:
            alert_text = l_alert.text
            if self.accept_next_alert:
                l_alert.accept()
            else:
                l_alert.dismiss()
            self.accept_next_alert = True
            self.logger.info("Alert: "+alert_text)
            return alert_text
        else:
            return None
    
    def get_param(self, name="username", check=None):
        try:
            return self.vars[name]
        except KeyError:
            if check:
                return None
            else:
                self.logger.error("Unexpected parameter {}!".format(name))
                raise KeyError
                
    def wait_until(self, how, where):
        self.wait.until(EC.element_to_be_clickable((how, where)))

    def logout(self):
        #BLOCK: Exit
        self.driver.find_element_by_css_selector("img[alt=\"Abmelden\"]").click()
        self.alert_check_and_logging()
        time.sleep(3)
       
    def logon(self):
        #BLOCK: Logon
        self.driver.find_element_by_id("sap-user").clear()
        self.driver.find_element_by_id("sap-user").send_keys(self.get_param("username"))
        self.driver.find_element_by_id("sap-password").clear()
        self.driver.find_element_by_id("sap-password").send_keys(self.get_param("password"))
        self.driver.find_element_by_id("b1").click()
        try:
            self.main_frame = self.driver.find_elements_by_tag_name("frame")[0]
        except Exception:
            self.logger.error("Fatal Error: Frame not found!")
        self.driver.switch_to_frame(self.main_frame)
        self.main_window = self.driver.current_window_handle
       
    def tearDown(self):
        self.driver.switch_to.window(self.main_window)
        self.driver.switch_to_frame(self.main_frame)
        self.logout()
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

