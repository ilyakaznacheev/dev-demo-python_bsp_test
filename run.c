#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[])
{
    char *script_name = "python run.py";
    char command[1000];
    strcpy(command, script_name);

    int i;
    for (i = 1; i < argc; i++)
    {
        strcat(command, " ");
        strcat(command, argv[i]);
    }
    system(command);
    return 0;
}
